﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;
using Castle.Core.Logging;
using YSoft.Rqa.Deployment;
using YSoft.Rqa.Deployment.Cli;
using YSoft.Rqa.Deployment.Gui;

namespace DemoApp
{
    public static class NativeMethods
    {
        [DllImport("Kernel32")]
        public static extern void AllocConsole();

        [DllImport("Kernel32")]
        public static extern void FreeConsole();
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : IDisposable
    {
        private const string ArtifactoryUrl = "artUrl";
        private const string ArtifactoryPath = "artPath";
        private readonly Ruda _ruda;
        private bool _disposed = false;

        public MainWindow(IEnumerable<string> args) {
            NativeMethods.AllocConsole();
            args = RudaHelper.HandleStartup(args);
            InitializeComponent();

            _ruda = new Ruda(new ArtifactoryDownloader(ArtifactoryUrl, ArtifactoryPath), new ConsoleLogger(nameof(Ruda)));
            if (args.Any()) {
                var rudaCli = new RudaCli(_ruda, new List<ICliExtension>(), new ConsoleLogger(nameof(RudaCli)));
                rudaCli.Parse(args);
                try {
                    Task.Run(() => rudaCli.ExecuteAsync()).Wait();
                } catch (Exception e) {
                    Console.WriteLine($"ex: {e.Message}{Environment.NewLine}{e.StackTrace}");
                }
                Application.Current.Shutdown();
                return;
            }

            tbTop.Text = $"Version: {_ruda.CurrentVersion()?.Version.ToNormalizedString() ?? "<empty>"}";
        }

        private void bShowGui_Click(object sender, RoutedEventArgs e) {
            new SelectPackage(_ruda, new ConsoleLogger(nameof(SelectPackage))).ShowDialog();
        }

        protected override void OnClosing(CancelEventArgs e) {
            base.OnClosing(e);
            Dispose();
        }

        public void Dispose() {
            _ruda?.Dispose();
            if (!_disposed) {
                _disposed = true;
                NativeMethods.FreeConsole();
            }
        }
    }
}
