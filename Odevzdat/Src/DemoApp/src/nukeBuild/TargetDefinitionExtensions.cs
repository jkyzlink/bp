﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Threading.Tasks;
using Nuke.Common;

static class TargetDefinitionExtensions
{
    public static ITargetDefinition ExecutesAsync(this ITargetDefinition target, Func<Task> action) {
        target.Executes(() => action().GetAwaiter().GetResult());
        return target;
    }
}
