﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using Nuke.Common;
using Nuke.Common.Tools.MSBuild;
using Nuke.Common.Tools.NuGet;
using Rqa.NukeHelpers;
using Ruda.Nuke;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using YSoft.Rqa.Deployment.Nuke;
using static Nuke.Common.Tools.MSBuild.MSBuildTasks;
using static Nuke.Common.Tools.DotNet.DotNetTasks;
using static Nuke.Common.Tools.NuGet.NuGetTasks;
using static Nuke.Common.Tooling.ProcessTasks;
using static Nuke.Common.IO.FileSystemTasks;
using static Nuke.Common.IO.PathConstruction;

// ReSharper disable InconsistentNaming
class Build : NukeBuild
{
    [Parameter("Skip clean during build (use for local builds).")]
    readonly bool NoClean;

    [Parameter("List of nuget source URLs. Separated with ','.", Separator = ",")]
    readonly string[] NugetSources = {"https://api.nuget.org/v3/index.json"};

    [Parameter("Artifactory url for RUDA usage.")]
    readonly string ArtifactoryUrl;

    [Parameter("Artifactory project path for RUDA usage.")]
    readonly string ArtifactoryProjectPath;

    [Parameter("Artifactory api key for RUDA usage.")]
    readonly string ArtifactoryApiKey;

    [Parameter("Deploy RUDA packages")]
    readonly bool SkipRudaDeploy;

    [Parameter("Cleanup RUDA repository")]
    readonly bool SkipRudaCleanup;

    readonly string[] Csprojs;

    AbsolutePath RudaOutputPath => OutputDirectory / "Ruda";

    public Build() {
        Csprojs = GlobFiles(SourceDirectory, "*.csproj")
            .Where(x => !x.Contains(".build.csproj"))
            .ToArray();
    }

    public static int Main() => Execute<Build>(x => x.Compile);

    Target Clean => _ => _
        .OnlyWhen(() => !NoClean)
        .Executes(() => {
            Verbosity = Verbosity.Verbose;
            MSBuild(x => DefaultMSBuild
                .SetTargets("Clean")
                .SetMSBuildVersion(MSBuildVersion.VS2017)
            );

            var dirs = GlobDirectories(SourceDirectory, "*/bin", "*/obj")
                .Where(x => !x.Contains("nukeBuild"));
            DeleteDirectories(dirs);
            EnsureCleanDirectories(new[] {OutputDirectory.ToString(), RudaOutputPath});
        });

    Target Generate_Metadata => _ => _
        .Executes(() => {
            Logger.Info($"Generated version {VersionCreation.Version.ToFullString()}");
            var projects = GlobFiles(SourceDirectory, "*.csproj")
                .Where(x => !x.Contains(".build"));
            foreach (var project in projects) {
                var doc = XDocument.Load(project);
                foreach (var v in doc.XPathSelectElements(@"/Project/PropertyGroup[* = Version]/Version"))
                    v.Value = VersionCreation.Version.ToFullString();
                doc.Save(project);
            }
        });

    Target Restore => _ => _
        .DependsOn(Clean)
        .Executes(() => {
            NuGetRestore(SolutionDirectory, settings => settings.SetSource(NugetSources));
        });

    Target Compile => _ => _
        .DependsOn(Restore)
        .DependsOn(Generate_Metadata)
        .Executes(() => {
            MSBuild(x => DefaultMSBuildCompile
                .SetTargets("Build")
                .SetMSBuildVersion(MSBuildVersion.VS2017));
        });

    Target Deploy => _ => _
        .DependsOn(Compile)
        .ExecutesAsync(async () => {
            if (string.IsNullOrEmpty(ArtifactoryApiKey))
                Logger.Warn($"{nameof(ArtifactoryApiKey)} is empty");
            var conf = new RudaSettings {
                CreateMsi = false,
                BranchId = VersionCreation.BranchId(VersionCreation.GitVersion.BranchName),
                BranchToId = VersionCreation.BranchId,
                ArtifactoryUrl = ArtifactoryUrl,
                ArtifactoryProjectPath = ArtifactoryProjectPath,
                ArtifactoryApiKey = ArtifactoryApiKey?.Substring(ArtifactoryApiKey.IndexOf(':') + 1) ?? "",
                OutputPath = OutputDirectory / "Ruda",
                SourceDirectory = SourceDirectory
            };
            RudaTasks.CreateNugetPackage(() => conf);
            await RudaTasks.DownloadOldPackages(() => conf);
            RudaTasks.CreateRudaPackage(() => conf);
            if (!SkipRudaDeploy)
                await RudaTasks.DeployRudaPackages(() => conf);
            if (!SkipRudaCleanup)
                await RudaTasks.RudaCleanupRemoteServer(() => conf);
        });
}
