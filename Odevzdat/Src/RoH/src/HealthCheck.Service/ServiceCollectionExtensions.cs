﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace YSoft.Rqa.HealthCheck.Service
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Registers <see cref="IRohApplication"/>, <see cref="IRohAppService{T}"/> and configures <see cref="RohApplicationOptions"/> in specified <see cref="IServiceCollection"/>
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection" /> to add services to.</param>
        /// <param name="options">The <see cref="RohApplicationOptions"/> configuration delegate.</param>
        /// <returns></returns>
        public static IServiceCollection AddRoh(
            this IServiceCollection services,
            Action<RohApplicationOptions> options) {
            services.Configure(options);
            services.TryAdd(ServiceDescriptor.Singleton<IRohApplication, RohApplication>());
            services.TryAdd(ServiceDescriptor.Singleton(typeof(IRohAppService<>), typeof(RohAppService<>)));
            return services;
        }
    }
}