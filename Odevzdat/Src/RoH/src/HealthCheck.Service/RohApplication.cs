﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Consul;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using YSoft.Rqa.HealthCheck.Core;

namespace YSoft.Rqa.HealthCheck.Service
{
    /// <summary>
    /// Applications it's health will be checked
    /// </summary>
    public interface IRohApplication
    {
        /// <summary>
        /// Register application to consul
        /// </summary>
        /// <returns></returns>
        Task RegisterApp();

        /// <summary>
        /// Deregister application from consul
        /// </summary>
        Task DeregisterApp();

        /// <summary>
        /// Application name (from options)
        /// </summary>
        string ApplicationName { get; }

        /// <summary>
        /// Node identification, usually hostname
        /// </summary>
        string NodeId { get; }

        /// <summary>
        /// Add <see cref="IRohAppService"/> as this application service and register it to consul
        /// </summary>
        /// <param name="appService"><see cref="IRohAppService"/> that has to be monitored by RoH</param>
        void AddAppService(IRohAppService appService);
    }

    public class RohApplication : IRohApplication
    {
        public string ApplicationName => _rohAppId.AppName;
        public string NodeId => _rohAppId.NodeId;

        private readonly ITagProvider _tagProvider;
        private readonly ILogger _log;
        private readonly IIpAddressProvider _addressProvider;
        private readonly RohServiceInstanceId _rohAppId;
        private readonly IConsulClient _consulClient;
        private readonly RohApplicationOptions _options;
        private readonly TcpServer _tcpServer;

        private readonly Dictionary<RohServiceInstanceId, IRohAppService> _appServices = new Dictionary<RohServiceInstanceId, IRohAppService>();

        public RohApplication(IOptions<RohApplicationOptions> options, IIpAddressProvider addressProvider = null,
            IConsulClient consulClient = null, ITagProvider tagProvider = null, ILogger<RohApplication> log = null) {
            _log = log ?? NullLogger<RohApplication>.Instance;
            _options = options.Value;
            _log.LogTrace("Using options: {RohApplicationOptions}", JsonConvert.SerializeObject(_options));
            _addressProvider = addressProvider ?? new RohIpAddressProvider(_options.ConsulClientOptions.Address);
            _consulClient = consulClient ?? ConsulUtil.InitializeConsulClient(_options.ConsulClientOptions);
            _tagProvider = tagProvider ?? new EmptyTagProvider();
            _rohAppId = new RohServiceInstanceId(_options.ApplicationName, string.Empty, nodeId: Dns.GetHostName());
            _tcpServer = new TcpServer(IPAddress.Any, _options.ApplicationHealthCheckPort);
        }

        /// <inheritdoc />
        public void AddAppService(IRohAppService appService) {
            var serviceId = new RohServiceInstanceId(_rohAppId.AppName, appService.Name, _rohAppId.NodeId);
            if (_appServices.ContainsKey(serviceId))
                throw new ArgumentException(
                    $"Service with name \'{appService.Name}\' is already registered.",
                    nameof(appService.Name)
                );
            appService.ServiceChanged += (s, a) => AppServiceOnServiceChanged(serviceId, appService);
            _appServices.Add(serviceId, appService);
            _log.LogDebug($"Service \'{appService.Name}\' has been added to the list of application services.");
        }

        private async void AppServiceOnServiceChanged(RohServiceId serviceId, IRohAppService appService) {
            try {
                _log.LogDebug($"Service \'{appService.Name}\' (\'{serviceId.ServiceId}\') has changed");
                await RegisterAppService(serviceId, appService);
            } catch (Exception e) {
                _log.LogError(e, $"Exception in {nameof(AppServiceOnServiceChanged)}.");
            }
        }

        private async Task RegisterAppService(RohServiceId serviceId, IRohAppService appService) {
            var serviceReg = appService.ServiceRegistration;
            serviceReg.Name = serviceId.ServiceId;
            serviceReg.ID = new RohServiceInstanceId(serviceId, NodeId).ServiceInstanceId;
            serviceReg.Tags = _options.StaticTags.Concat(serviceReg.Tags).ToArray();
            if (string.IsNullOrEmpty(serviceReg.Address))
                serviceReg.Address = _options.RegisterHostname ? Dns.GetHostName() :
                    _addressProvider.IpAddress.ToString();

            serviceReg.Checks = serviceReg.Checks.Append(new AgentServiceCheck {
                DeregisterCriticalServiceAfter = TimeSpan.FromHours(2),
                TCP = IpExtensions.GetFormattedIpEndpoint(serviceReg.Address, serviceReg.Port),
                Interval = TimeSpan.FromSeconds(2),
                Timeout = TimeSpan.FromSeconds(7)
            }).ToArray();

            try {
                await _consulClient.Agent.ServiceRegister(serviceReg).ConfigureAwait(false);
                _log.LogInformation($"Service {serviceReg.Name} succesfully registered to Consul");
            }
            catch (Exception e) {
                _log.LogError(e, $"Error while registering serviceInstance {serviceReg.Name}");
            }
        }

        /// <summary>
        /// Starts TCp server and registers application to consul
        /// </summary>
        /// <returns></returns>
        public async Task RegisterApp() {
            _tcpServer.Start();
            _log.LogInformation($"Health check TCP server started on port {_tcpServer.Port}.");
            var healthCheckAddr = _options.RegisterHostname ? Dns.GetHostName() : _addressProvider.IpAddress.ToString();
            var healthCheckEndpoint = IpExtensions.GetFormattedIpEndpoint(healthCheckAddr, _tcpServer.Port ?? -1);
            _log.LogInformation($"Using healthcheck enpoint \'{healthCheckEndpoint}\'.");
            var registration = new AgentServiceRegistration {
                Name = _rohAppId.AppName,
                ID = _rohAppId.ServiceInstanceId,
                Address = IpExtensions.GetFormattedIpAddress(healthCheckAddr),
                Port = _tcpServer.Port ?? 0,
                Tags = _options.StaticTags.Concat(_tagProvider.Tags.Select(x => x.RohValue)).ToArray(),
                Checks = new[] {
                    new AgentServiceCheck {
                        DeregisterCriticalServiceAfter = TimeSpan.FromHours(24),
                        TCP = healthCheckEndpoint,
                        Interval = _options.CheckInterval,
                        Timeout = _options.CheckTimeout
                    }
                }
            };

            try {
                await _consulClient.Agent.ServiceRegister(registration).ConfigureAwait(false);
                _log.LogInformation($"Application \'{ApplicationName}\' succesfully registered to Consul");
            } catch (Exception e) {
                _log.LogError(e, $"Error while registering application \'{ApplicationName}\'");
            }
        }

        /// <summary>
        /// Deregister application from consul and stop TCP server
        /// </summary>
        public async Task DeregisterApp() {
            try {
                _tcpServer.Stop();
                if (!_tcpServer.ServerTask.Wait(TimeSpan.FromSeconds(1)))
                    _log.LogError("TCP server was not closed within defined interval");

                await _consulClient.Agent.ServiceDeregister(_rohAppId.ServiceInstanceId).ConfigureAwait(false);
                _log.LogInformation($"Application {ApplicationName} succesfully deregistered from Consul");
            }
            catch (Exception e) {
                _log.LogInformation(e, $"Error while deregistering application {ApplicationName}");
            }
        }
    }
}
