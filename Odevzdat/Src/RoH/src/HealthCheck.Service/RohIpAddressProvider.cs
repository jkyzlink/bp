﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Net;
using System.Net.Sockets;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;

namespace YSoft.Rqa.HealthCheck.Service
{
    internal class RohIpAddressProvider : IIpAddressProvider
    {

        public IPAddress IpAddress => ipAddress.Value;

        private readonly Lazy<IPAddress> ipAddress;
        private readonly Uri _consulUri;
        private readonly ILogger _log;

        public RohIpAddressProvider(string consulAddress, ILogger<RohIpAddressProvider> log = null) {
            _consulUri = new Uri(consulAddress);
            _log = log ?? NullLogger<RohIpAddressProvider>.Instance;
            ipAddress = new Lazy<IPAddress>(GetIpAddress);
        }

        private IPAddress GetIpAddress() {
            //Try IPv6
            using (var socket = new Socket(AddressFamily.InterNetworkV6, SocketType.Dgram, 0)) {
                try {
                    socket.Connect(_consulUri.Host, _consulUri.Port);
                    var endPoint = (IPEndPoint) socket.LocalEndPoint;
                    _log.LogInformation($"Got local IPv6 address: {endPoint.Address}");
                    return endPoint.Address;
                } catch (Exception) {
                    //No valid ipv6 address
                }
            }

            //Try IPv4
            using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0)) {
                try {
                    socket.Connect(_consulUri.Host, _consulUri.Port);
                    var endPoint = (IPEndPoint) socket.LocalEndPoint;
                    _log.LogInformation($"Got local IPv4 address: {endPoint.Address}");
                    return endPoint.Address;
                } catch (Exception) {
                    //No valid ipv4 address
                }
            }

            const string msg = "Unable to get local IP Address, consul registration will not work.";
            _log.LogError(msg);
            throw new Exception(msg);
        }
    }
}
