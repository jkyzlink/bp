﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Collections.ObjectModel;
using YSoft.Rqa.HealthCheck.Core;

namespace YSoft.Rqa.HealthCheck.Service
{
    /// <summary>
    /// Provides no tags, use primary for testing
    /// </summary>
    public class EmptyTagProvider : ITagProvider
    {
        /// <summary>
        /// Collection witout items
        /// </summary>
        public ObservableCollection<RohTag> Tags { get; } = new ObservableCollection<RohTag>();
    }

    /// <summary>
    /// Foundation that can be used to create new <see cref="ITagProvider"/>
    /// </summary>
    public abstract class BaseTagProvider : ITagProvider
    {
        /// <inheritdoc />
        [Obsolete("Do not use! Use _tags")]
        public ObservableCollection<RohTag> Tags => _tags;

        /// <summary>
        /// Observable range collection that allow range operations
        /// </summary>
        protected readonly ObservableRangeCollection<RohTag> _tags = new ObservableRangeCollection<RohTag>();
    }
}
