﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System.Net;

namespace YSoft.Rqa.HealthCheck.Service
{
    /// <summary>
    /// Provides an IP Addresse
    /// </summary>
    public interface IIpAddressProvider
    {
        /// <summary>
        /// Gets IP Address
        /// </summary>
        /// <returns>IPAddress</returns>
        IPAddress IpAddress { get; }
    }
}
