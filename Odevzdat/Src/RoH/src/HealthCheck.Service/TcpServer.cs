﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace YSoft.Rqa.HealthCheck.Service
{
    /// <summary>
    /// SimpleTCP server that just accept a connection and immediately close it. 
    /// </summary>
    public class TcpServer : IDisposable
    {
        private readonly TcpListener _listener;
        private CancellationTokenSource _tokenSource;
        private CancellationToken _token;

        /// <summary>
        /// Creates new <see cref="TcpListener"/> on specified <paramref name="address"/> and <paramref name="port"/>
        /// </summary>
        /// <param name="address">An <see cref="T:System.Net.IPAddress"></see> that represents the local IP address.</param>
        /// <param name="port">The port on which to listen for incoming connection attempts.</param>
        public TcpServer(IPAddress address, int port) {
            _listener = new TcpListener(address, port);
        }

        /// <summary>
        /// Determine, if the server is running or not.
        /// </summary>
        public bool Listening { get; private set; }

        /// <summary>
        /// If listenin returns port on which the server currently listen otherwise null.
        /// </summary>
        public int? Port => !Listening ? null : (_listener.LocalEndpoint as IPEndPoint)?.Port;

        /// <summary>
        /// Server task responsible for accepting tcp connections
        /// </summary>
        public Task ServerTask;

        /// <summary>
        /// Starts listening for incoming connection requests.
        /// </summary>
        public void Start() {
            StartListening();
            ServerTask = ServerTaskImpl();
        }

        /// <summary>
        /// Starts listening for incoming connection requests and .
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public Task StartAsync(CancellationToken? token = null) {
            StartListening();
            return ServerTask = ServerTaskImpl(token);
        }

        private void StartListening() {
            if (Listening)
                throw new InvalidOperationException("Server is already started");
            _listener.Start();
            Listening = true;
        }

        private async Task ServerTaskImpl(CancellationToken? token = null) {
            _tokenSource = CancellationTokenSource.CreateLinkedTokenSource(token ?? CancellationToken.None);
            _token = _tokenSource.Token;

            using (var registration = _token.Register(() => _listener.Stop())) {
                try {
                    while (!_token.IsCancellationRequested) {
                        var tcpClient = await _listener.AcceptTcpClientAsync().ConfigureAwait(false);
                        tcpClient.Close();
                    }

                    _token.ThrowIfCancellationRequested();
                }
                // task was cancelled
                catch (ObjectDisposedException) {}
                catch (InvalidOperationException) {}
                catch (OperationCanceledException) { }
                finally {
                    _listener.Stop();
                    Listening = false;
                }
            }
        }

        /// <summary>
        /// Closes the listener.
        /// </summary>
        public void Stop() => _tokenSource?.Cancel();

        /// <summary>
        /// Closes the listener.
        /// </summary>
        public void Dispose() => Stop();
    }
}