﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Linq;
using System.Threading.Tasks;
using Consul;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;

namespace YSoft.Rqa.HealthCheck.Service
{
    public interface IRohAppService
    {
        int? ServicePort { get; set; }
        string ServiceAddress { get; set; }
        event EventHandler ServiceChanged;
        string Name { get; }
        bool Enabled { get; }
        AgentServiceRegistration ServiceRegistration { get; }
        Task EnableCheck();
        Task DisableCheck();
    }

    public interface IRohAppService<T> : IRohAppService
    { }

    public class RohAppService<T> : RohAppService, IRohAppService<T> where T : class
    {

        public RohAppService(IOptions<RohApplicationOptions> appOptions, IRohApplication rohApplication, IServiceProvider serviceProvider,
            ILogger<RohAppService<T>> log) :
            base(serviceProvider, log ?? NullLogger<RohAppService<T>>.Instance) {

            var serviceOptions = appOptions.Value.RohAppServices.SingleOrDefault(x => x.ServiceType == typeof(T));

            Options = serviceOptions ?? throw new ArgumentException($"Healh check for service \'{typeof(T).Name}\' was not registered.");
            rohApplication.AddAppService(this);
        }
    }

    public class RohAppService : IRohAppService
    {
        public string Name { get; private set; }

        public int? ServicePort {
            get => _servicePort;
            set {
                _servicePort = value;
                ServiceChanging?.Invoke(this, EventArgs.Empty);
            }
        }

        public string ServiceAddress {
            get => _serviceAddress;
            set {
                _serviceAddress = value;
                ServiceChanging?.Invoke(this, EventArgs.Empty);
            }
        }


        public event EventHandler ServiceChanged;
        public bool Enabled { get; private set; }
        public AgentServiceRegistration ServiceRegistration { get; private set; } = new AgentServiceRegistration();

        internal RohAppServiceOptions Options {
            get => _options;
            set {
                _options = value;
                Name = _options.ServiceName;
                if (_options.TagProviderFactory != null && _options.TagProvider == null) {
                    _log.LogDebug("Tag provider factory was provided, invoking factory method");
                    try {
                        _options.TagProvider = _options.TagProviderFactory.Invoke(_serviceProvider);
                    } catch (Exception e) {
                        _log.LogError(e, "Exception during TagProvider instantiation");
                        throw;
                    }
                }
                else if (_options.TagProvider == null)
                    throw new ArgumentNullException($"Set {nameof(_options.TagProviderFactory)} or {nameof(_options.TagProvider)}");

                _options.TagProvider.Tags.CollectionChanged += (s, a) => ServiceChanging?.Invoke(this, EventArgs.Empty);
            }
        }

        private event EventHandler ServiceChanging;
        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger _log;
        private RohAppServiceOptions _options;
        private int? _servicePort;
        private string _serviceAddress;

        internal RohAppService(IServiceProvider serviceProvider, ILogger log) {
            _serviceProvider = serviceProvider;
            _log = log;
            ServiceChanging += (s, a) => {
                UpdateServiceRegistration();
                ServiceChanged?.Invoke(this, EventArgs.Empty);
            };
        }

        private void UpdateServiceRegistration() {
            if(!ServicePort.HasValue)
                throw new ArgumentNullException("Service must set healtcheck port", nameof(ServicePort));
            ServiceRegistration.Port = ServicePort.Value;
            ServiceRegistration.Address = ServiceAddress;
            ServiceRegistration.Tags = Options.TagProvider.Tags.Select(x => x.RohValue).ToArray();
            ServiceRegistration.Checks = Options.ServiceChecks.ToArray();
        }

        public async Task EnableCheck() {
            await Task.CompletedTask;
            Enabled = true;
            _log.LogInformation($"Service \'{Options.ServiceName}\' implemented by {Options.ServiceType} has been enabled.");
            ServiceChanging?.Invoke(this, EventArgs.Empty);
        }

        public async Task DisableCheck() {
            await Task.CompletedTask;
            Enabled = false;
            _log.LogInformation($"Service \'{Options.ServiceName}\' implemented by {Options.ServiceType} has been DEregistered.");
            ServiceChanging?.Invoke(this, EventArgs.Empty);
        }
    }
}