﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Collections.Generic;
using Consul;
using Newtonsoft.Json;
using YSoft.Rqa.HealthCheck.Core;

namespace YSoft.Rqa.HealthCheck.Service
{
    public class RohAppServiceOptions
    {
        public string ServiceName { get; set; }

        /// <summary>
        /// If null use application setting; if set use hostname instead of an ip address for service registration
        /// </summary>
        public bool? RegisterHostname { get; set; }

        public Type ServiceType { get; set; }

        public ITagProvider TagProvider { get; set; }

        [JsonIgnore]
        public Func<IServiceProvider, ITagProvider> TagProviderFactory { get; set; }

        public IList<AgentServiceCheck> ServiceChecks { get; set; } = new List<AgentServiceCheck>();
    }

    public class RohApplicationOptions
    {
        /// <summary>
        /// Additional consul client configuration
        /// </summary>
        public ConsulClientOptions ConsulClientOptions { get; set; } = new ConsulClientOptions();

        /// <summary>
        /// Health check interval
        /// </summary>
        public TimeSpan CheckInterval { get; set; } = TimeSpan.FromSeconds(10);

        /// <summary>
        /// Health check timeout
        /// </summary>
        public TimeSpan CheckTimeout { get; set; } = TimeSpan.FromSeconds(3);

        /// <summary>
        /// Use hostname instead of an ip address for service registration
        /// </summary>
        public bool RegisterHostname { get; set; } = false;

        /// <summary>
        /// Additional flags added during service registration
        /// </summary>
        public List<string> StaticTags { get; set; } = new List<string>();

        /// <summary>
        /// Application name used for registration
        /// </summary>
        public string ApplicationName { get; set; }

        /// <summary>
        /// Port used to determine if the application is running
        /// </summary>
        public int ApplicationHealthCheckPort { get; set; } = 0;

        public IList<RohAppServiceOptions> RohAppServices { get; set; } = new List<RohAppServiceOptions>();
    }
}
