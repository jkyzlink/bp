﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System.Collections.ObjectModel;
using YSoft.Rqa.HealthCheck.Core;

namespace YSoft.Rqa.HealthCheck.Service
{
    /// <summary>
    /// Provides tags to <see cref="IRohAppService{T}"/>
    /// </summary>
    public interface ITagProvider
    {
        /// <summary>
        /// <see cref="RohTag"/> collection used in consul
        /// </summary>
        ObservableCollection<RohTag> Tags { get; }
    }
}
