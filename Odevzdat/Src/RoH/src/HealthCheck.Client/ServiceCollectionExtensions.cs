﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using YSoft.Rqa.HealthCheck.Core;

namespace YSoft.Rqa.HeatlhCheck.Client
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Registers <see cref="IRohClient"/> and configures <see cref="ConsulClientOptions"/> in specified <see cref="IServiceCollection"/>
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection" /> to add services to.</param>
        /// <param name="options">The <see cref="ConsulClientOptions"/> configuration delegate.</param>
        /// <returns></returns>
        public static IServiceCollection AddRohClient(this IServiceCollection services, Action<ConsulClientOptions> options) {
            services.Configure(options);
            services.TryAdd(ServiceDescriptor.Transient<IRohClient, RohClient>());
            return services;
        }
    }
}