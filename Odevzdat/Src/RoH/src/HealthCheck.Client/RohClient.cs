﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Consul;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using YSoft.Rqa.HealthCheck.Core;

namespace YSoft.Rqa.HeatlhCheck.Client
{
    public class RohService
    {
        public virtual RohServiceId ServiceId { get; protected set; }
        public IEnumerable<RohTag> Tags { get; protected set; }

        public RohService(RohServiceId serviceId, IEnumerable<RohTag> tags) {
            ServiceId = serviceId;
            Tags = tags;
        }

        protected RohService() { }

    }

    public class RohServiceInstance : RohService
    {
        public override RohServiceId ServiceId => ServiceInstanceId;
        public RohServiceInstanceId ServiceInstanceId { get; protected set; }
        public string Address { get; protected set; }
        public int Port { get; protected set; }
        public IEnumerable<Consul.HealthCheck> Checks { get; protected set; }

        public RohServiceInstance(RohServiceInstanceId instanceId, IEnumerable<RohTag> tags, string address, int port, IEnumerable<Consul.HealthCheck> checks) :
            base(instanceId, tags) {
            ServiceInstanceId = instanceId;
            Address = address;
            Port = port;
            Checks = checks;
        }

        public RohServiceInstance(ServiceEntry catalogEntry) {
            ServiceInstanceId = RohIdParser.Parse<RohServiceInstanceId>(catalogEntry.Service.ID);
            Tags = catalogEntry.Service.Tags.Select(t => new RohTag(t));
            Address = catalogEntry.Service.Address;
            Port = catalogEntry.Service.Port;
            Checks = catalogEntry.Checks;

        }
    }

    public abstract class RohClientEndpoint
    {
        internal readonly IConsulClient Client;

        public RohClientEndpoint(IConsulClient client) {
            Client = client;
        }
    }

    public static class ConsulClientExtensions
    {
        public static T AssertSuccess<T>(this QueryResult<T> queryResult) {
            if (queryResult.StatusCode != HttpStatusCode.OK)
                throw new RohClientException($"Invalid status code, status code != Ok, status code = {queryResult.StatusCode}");
            return queryResult.Response;
        }
    }

    public class RohServiceEndpoint : RohClientEndpoint
    {
        private readonly ILogger _log;

        public RohServiceEndpoint(IConsulClient client, ILogger log) : base(client) {
            _log = log;
        }

        public async Task<IEnumerable<RohService>> All() {
            var services = (await Client.Catalog.Services().ConfigureAwait(false)).AssertSuccess();
            var ret = new List<RohService>();
            foreach (var service in services) {
                try {
                    var tags = service.Value.Select(x => new RohTag(x));
                    ret.Add(new RohService(RohIdParser.Parse<RohServiceId>(service.Key), tags));
                } catch (Exception) {
                    _log.LogDebug($"Service ID \'{service.Key}\' is not valid roh service ID");
                }
            }

            return ret;
        }

        public Task<IEnumerable<RohServiceInstance>> Service(string appName, string serviceName, IEnumerable<string> tags = null) {
            return Service(appName, serviceName, false, tags);
        }

        public Task<IEnumerable<RohServiceInstance>> Service(string appName, string serviceName, bool passingOnly, IEnumerable<string> tags = null) {
            return Service(new RohServiceId(appName, serviceName), passingOnly, tags);
        }

        public Task<IEnumerable<RohServiceInstance>> Service(RohServiceId serviceId, IEnumerable<string> tags = null) {
            return Service(serviceId, false, tags);
        }

        public async Task<IEnumerable<RohServiceInstance>> Service(RohServiceId serviceId, bool passingOnly, IEnumerable<string> tags = null) {
            var services = (await Client.Health.Service(serviceId.ServiceId, string.Empty, passingOnly).ConfigureAwait(false)).AssertSuccess();
            var ret = new List<RohServiceInstance>();
            foreach (var service in services) {
                try {
                    ret.Add(new RohServiceInstance(service));
                } catch (Exception) {
                    _log.LogDebug($"Service with ID \'{service.Service.ID}\' is not valid roh serviceInstance");
                }
            }
            return ret;
        }
    }

    public interface IRohClient {
        RohServiceEndpoint Services { get; }
        IConsulClient Client { get; }
    }

    public class RohClient : IRohClient
    {
        public RohServiceEndpoint Services => _services.Value;

        public IConsulClient Client { get; }

        private readonly ConsulClientOptions _options;
        private readonly ILogger<RohClient> _log;
        private readonly Lazy<RohServiceEndpoint> _services;

        public RohClient(IOptions<ConsulClientOptions> options, ILogger<RohClient> log = null) {
            _options = options.Value;
            _log = log ?? NullLogger<RohClient>.Instance;
            _log.LogTrace($"Configured RoH client options: {JsonConvert.SerializeObject(_options)}");
            Client = ConsulUtil.InitializeConsulClient(_options);
            _services = new Lazy<RohServiceEndpoint>(()=> new RohServiceEndpoint(Client, _log));
        }



        //Services
        //Applications
    }
}