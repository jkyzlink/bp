﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Runtime.Serialization;

namespace YSoft.Rqa.HeatlhCheck.Client {
    public class RohClientException : Exception
    {
        public RohClientException()
        {
        }

        public RohClientException(string message) : base(message)
        {
        }

        public RohClientException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected RohClientException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}