﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.MSBuild;
using Nuke.Common.Tools.NuGet;
using Nuke.Common;
using Nuke.Common.Tooling;
using Nuke.Common.Utilities.Collections;
using Rqa.NukeHelpers;
using static Nuke.Common.Tools.MSBuild.MSBuildTasks;
using static Nuke.Common.Tools.DotNet.DotNetTasks;
using static Nuke.Common.Tools.NuGet.NuGetTasks;
using static Nuke.Common.IO.FileSystemTasks;
using static Nuke.Common.IO.PathConstruction;

// ReSharper disable InconsistentNaming

class Build : NukeBuild
{
    // Auto-injection fields:
    //  - [GitVersion] must have 'GitVersion.CommandLine' referenced
    //  - [GitRepository] parses the origin from git config
    //  - [Parameter] retrieves its value from command-line arguments or environment variables
    //
    //[GitVersion] readonly GitVersion GitVersion;
    //[GitRepository] readonly GitRepository GitRepository;
    //[Parameter] readonly string MyGetApiKey;
    [Parameter("ApiKey for nuget repository.")]
    readonly string NugetApiKey;

    [Parameter("Nuget repository push url (optional)")]
    readonly string NugetPushUrl;

    [Parameter("List of nuget source URLs. Separated with ','.", Separator = ",")]
    readonly string[] NugetSources = {"https://api.nuget.org/v3/index.json"};

    [Parameter("Sets file and product version in output dlls")]
    readonly bool SetBinaryVersion;

    AbsolutePath NugetOutputPath => OutputDirectory / "Nugets";

    // This is the application entry point for the build.
    // It also defines the default target to execute.
    public static int Main() => Execute<Build>(x => x.Create_Nugets);

    Target Clean => _ => _
        .Executes(() =>
        {
            Verbosity = Verbosity.Verbose;
            MSBuild(x => DefaultMSBuild
                .SetTargets("Clean")
                .SetMSBuildVersion(MSBuildVersion.VS2017)
            );

            var dirs = GlobDirectories(SourceDirectory, "*/bin", "*/obj")
                .Where(x => !x.Contains("nukeBuild"));
            DeleteDirectories(dirs);
            EnsureCleanDirectories(new[] {OutputDirectory, NugetOutputPath.ToString()});
        });

    Target Restore => _ => _
        .DependsOn(Clean)
        .Executes(() =>
        {
            NuGetRestore(SolutionDirectory, settings => settings.SetSource(NugetSources));
        });

    Target Compile => _ => _
        .DependsOn(Clean)
        .DependsOn(Restore)
        .Executes(() =>
        {
            var ver = new Version(1, 0).ToString();
            if (SetBinaryVersion) {
                var semVer = VersionCreation.Version;
                ver = new Version(semVer.Major, semVer.Minor, semVer.Patch, 0).ToString();
            }

            MSBuild(x => DefaultMSBuildCompile
                .SetTargets("Build")
                .SetMSBuildVersion(MSBuildVersion.VS2017)
                .SetAssemblyVersion(ver)
                .SetFileVersion(ver)
                .SetInformationalVersion(ver)
            );
        });

    Target Create_Nugets => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            foreach (var csproj in NugetHelpers.CreateAllNuspecsFromSpecs()) {
                if (SolutionHelpers.IsCoreCsproj(csproj)) {
                    Logger.Info($"Packing project {csproj.ProjectName} using dotnet pack");
                    DotNetPack(cfg => cfg
                        .SetProject(csproj.AbsolutePath)
                        .SetOutputDirectory(NugetOutputPath)
                        .SetWorkingDirectory(SourceDirectory)
                        .SetProperties(new Dictionary<string, object>()
                            .AddKeyValue("AssemblyVersion", "1.0.0.0")
                        )
                        .SetVersion(VersionCreation.Version.ToNormalizedString())
                        .SetConfiguration(Configuration)
                    );
                }
                else {
                    Logger.Info($"Packing project {csproj.ProjectName} using nuget pack");
                    NuGetPack(
                        csproj.AbsolutePath,
                        VersionCreation.Version.ToNormalizedString(),
                        settings => settings
                            .SetOutputDirectory(NugetOutputPath)
                            .SetBasePath(Path.GetDirectoryName(csproj.AbsolutePath))
                            .SetWorkingDirectory(Path.GetDirectoryName(csproj.AbsolutePath))
                            .EnableIncludeReferencedProjects()
                            .EnableForceEnglishOutput()
                            .SetConfiguration(Configuration)
                    );
                }
            }
        });
}
