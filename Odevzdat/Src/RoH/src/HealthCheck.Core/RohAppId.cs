﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

namespace YSoft.Rqa.HealthCheck.Core
{
    public class RohAppId
    {
        public readonly string AppName;
        public string AppId => AppName;
        public virtual string ConsulId => $"{AppName}-";

        public RohAppId(string appName) {
            AppName = appName;
        }

        public override string ToString() => ConsulId;
    }
}