﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

namespace YSoft.Rqa.HealthCheck.Core
{
    public enum TagVisibility
    {
        /// <summary>
        /// Tag won't be visible on RoH dashborad
        /// </summary>
        Private,

        /// <summary>
        /// Tag will be visible on RoH dashborad
        /// </summary>
        Public
    }
}