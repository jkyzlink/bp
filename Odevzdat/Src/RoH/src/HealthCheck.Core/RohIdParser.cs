﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Text.RegularExpressions;

namespace YSoft.Rqa.HealthCheck.Core
{
    public static class RohIdParser
    {
        public static T Parse<T>(string rohId) where T : RohAppId {
            var ret = Parse(rohId);
            return ret as T ?? throw new FormatException($"Id {ret.GetType().Name} cannot be used as {typeof(T).Name}");
        }

        public static RohAppId Parse(string rohId) {
            var exMsg = $"Invalid Roh Id.{Environment.NewLine}\'{rohId}\' does not appear to be in valid format.";
            if (string.IsNullOrEmpty(rohId))
                throw new ArgumentNullException(nameof(rohId));

            var rx = new Regex(
                @"^(?'app'[a-zA-Z0-9]*?)\-(?:(?'service'[a-zA-Z0-9]*?)?(?:\-(?'nodeId'[a-zA-Z0-9-]+))?)?$");
            var match = rx.Match(rohId);
            if (!match.Success)
                throw new FormatException(exMsg);

            var m = (app: match.Groups["app"].Value, svc: match.Groups["service"].Value,
                node: match.Groups["nodeId"].Value);

            if (!match.Groups["app"].Success)
                throw new FormatException($"{exMsg} AppName missing");

            if (string.IsNullOrEmpty(m.app) && string.IsNullOrEmpty(m.svc))
                throw new FormatException($"{exMsg} Node or service Id missing");

            if ((!match.Groups["service"].Success || string.IsNullOrEmpty(m.svc)) && string.IsNullOrEmpty(m.node))
                return new RohAppId(m.app);

            if (!match.Groups["nodeId"].Success)
                return new RohServiceId(m.app, m.svc);

            return new RohServiceInstanceId(m.app, m.svc, m.node);
        }
    }
}