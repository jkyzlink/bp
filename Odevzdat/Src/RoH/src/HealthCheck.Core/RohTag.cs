﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Diagnostics;

namespace YSoft.Rqa.HealthCheck.Core
{
    [DebuggerDisplay("{Value} ({Visibility})")]
    public struct RohTag
    {
        private const string PubPrefix = "-Pub";

        /// <summary>
        /// Tag visibility
        /// </summary>
        public TagVisibility Visibility;

        /// <summary>
        /// RAW Tag value used in consul
        /// </summary>
        public string RohValue => Decorate(Visibility, Value);

        /// <summary>
        /// Tag value without visibility modifiers
        /// </summary>
        public string Value;

        /// <summary>
        /// Instantiate <see cref="RohTag"/> from <see cref="TagVisibility"/> and tag value
        /// </summary>
        /// <param name="visibility">Tag visibility</param>
        /// <param name="value">Tag value (without visibility modifiers)</param>
        public RohTag(TagVisibility visibility, string value) {
            Visibility = visibility;
            Value = value;
        }

        /// <summary>
        /// Instantiate <see cref="RohTag"/> from RAW RohValue used in consul
        /// </summary>
        /// <param name="rohValue">RAW Tag value with visibility modifiers</param>
        public RohTag(string rohValue) {
            if (rohValue.StartsWith(PubPrefix)) {
                Visibility = TagVisibility.Public;
                Value = rohValue.Substring(PubPrefix.Length);
            }
            else {
                Visibility = TagVisibility.Private;
                Value = rohValue;
            }
        }

        private static string Decorate(TagVisibility visibility, string value) {
            switch (visibility) {
                case TagVisibility.Private:
                    return value;
                case TagVisibility.Public:
                    return $"{PubPrefix}{value}";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public override string ToString() => Value;
    }
}
