﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

namespace YSoft.Rqa.HealthCheck.Core
{
    /// <summary>
    /// Consul options
    /// </summary>
    public class ConsulClientOptions
    {
        /// <summary>
        /// Conusul server address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Datacenter name
        /// </summary>
        public string Datacenter { get; set; }

        /// <summary>
        /// Consul token
        /// </summary>
        public string Token { get; set; }
    }
}
