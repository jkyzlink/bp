﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System.Net;
using System.Net.Sockets;

namespace YSoft.Rqa.HealthCheck.Core
{
    /// <summary>
    /// Some IP extension methods
    /// </summary>
    public static class IpExtensions
    {
        /// <summary>
        /// Returns well formatted IP endpoint. Support address in IPv4, IPv6 and host format.
        /// </summary>
        /// <param name="address">Specified address</param>
        /// <param name="port">specified port</param>
        /// <returns>Well formatted IP endpoint.</returns>
        public static string GetFormattedIpEndpoint(string address, int port) {
            if (IPAddress.TryParse(address, out var ipAddress))
                return new IPEndPoint(ipAddress, port).ToString();
            else {
                //DNS Endpoint's ToString sucks
                var endpoint = new DnsEndPoint(address, port);
                return $"{endpoint.Host}:{endpoint.Port}";
            }
        }

        /// <summary>
        /// Returns well formatted IP address, works for both IPv4 and IPv6.
        /// </summary>
        /// <returns>Well formatted IP address</returns>
        public static string GetFormattedIpAddress(string address) {
            var ip = IPAddress.Parse(address);
            if (ip.AddressFamily == AddressFamily.InterNetworkV6)
                return $"[{ip}]";

            return ip.ToString();
        }
    }
}