﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using Consul;

namespace YSoft.Rqa.HealthCheck.Core
{
    public static class ConsulUtil
    {
        public static IConsulClient InitializeConsulClient(ConsulClientOptions opts) {
            var client = new ConsulClient {
                Config = {
                    Address = new Uri(opts.Address),
                    Datacenter = opts.Datacenter,
                    Token = opts.Token
                }
            };
            return client;
        }
    }
}