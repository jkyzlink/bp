﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;

namespace YSoft.Rqa.HealthCheck.Core
{
    public class RohServiceInstanceId : RohServiceId
    {
        public readonly string NodeId;
        public string ServiceInstanceId => $"{AppName}-{ServiceName}-{NodeId}";
        public override string ConsulId => $"{AppName}-{ServiceName}-{NodeId}";

        public RohServiceInstanceId(string appName, string serviceName, string nodeId) : base(appName,
            serviceName ?? string.Empty) {
            if (string.IsNullOrEmpty(nodeId))
                throw new ArgumentNullException(nameof(nodeId));
            NodeId = nodeId;
        }

        public RohServiceInstanceId(RohServiceId serviceId, string nodeId) : base(serviceId, serviceId.ServiceName) {
            if (string.IsNullOrEmpty(nodeId))
                throw new ArgumentNullException(nameof(nodeId));
            NodeId = nodeId;
        }
    }
}
