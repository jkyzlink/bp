﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

namespace YSoft.Rqa.HealthCheck.Core
{
    public class RohServiceId : RohAppId
    {
        public readonly string ServiceName;
        public string ServiceId => $"{AppName}-{ServiceName}";
        public override string ConsulId => $"{AppName}-{ServiceName}";

        public RohServiceId(string appName, string serviceName) : base(appName) {
            ServiceName = serviceName;
        }

        public RohServiceId(RohAppId appId, string serviceName) : base(appId.AppName) {
            ServiceName = serviceName;
        }
    }
}