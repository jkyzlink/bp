/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using NUnit.Framework;
using YSoft.Rqa.HealthCheck.Core;
using YSoft.Rqa.HeatlhCheck.Client;

namespace HealthCheck.Tests
{
    [TestFixture]
    public class RohServiceIdTests
    {
        [Test]
        [TestCase("app--node", "app", "", "node")]
        [TestCase("app-svc-node", "app", "svc", "node")]
        [TestCase("app-svc-node-1", "app", "svc", "node-1")]
        [TestCase("app-svc--node-1", "app", "svc", "-node-1")]
        [TestCase("-svc--node-1", "", "svc", "-node-1")]
        [TestCase("-s--", "", "s", "-")]
        [TestCase("a---", "a", "", "-")]
        public void ShouldParseRohServiceInstanceId(string rohId, string app, string service, string node)
        {
            var serviceId = RohIdParser.Parse<RohServiceInstanceId>(rohId);
            Assert.That(serviceId, Is.TypeOf<RohServiceInstanceId>());
            Assert.AreEqual(serviceId.ConsulId, rohId);
            Assert.AreEqual(serviceId.AppId, app);
            Assert.AreEqual(serviceId.ServiceName, service);
            Assert.AreEqual(serviceId.NodeId, node);
        }

        [Test]
        [TestCase("-svc", "", "svc")]
        [TestCase("app-svc", "app", "svc")]
        public void ShouldParseRohServiceId(string rohId, string app, string service)
        {
            var serviceId = RohIdParser.Parse<RohServiceId>(rohId);
            Assert.That(serviceId, Is.TypeOf<RohServiceId>());
            Assert.AreEqual(serviceId.ConsulId, rohId);
            Assert.AreEqual(serviceId.AppId, app);
            Assert.AreEqual(serviceId.ServiceName, service);
        }

        [Test]
        [TestCase("--")]
        [TestCase("app-")]
        [TestCase("app--")]
        public void ShouldNotParseRohServiceId(string rohId) {
            Assert.Throws<FormatException>(() => RohIdParser.Parse<RohServiceId>(rohId));
        }

        [Test]
        [TestCase("app-", "app")]
        public void ShouldParseRohAppId(string rohId, string app)
        {
            var serviceId = RohIdParser.Parse<RohAppId>(rohId);
            Assert.That(serviceId, Is.TypeOf<RohAppId>());
            Assert.AreEqual(serviceId.ConsulId, rohId);
            Assert.AreEqual(serviceId.AppId, app);
        }

        [Test]
        [TestCase("app--")]
        public void ShouldNotParseRohAppId(string rohId)
        {
            Assert.Throws<FormatException>(() => RohIdParser.Parse<RohAppId>(rohId));
        }

        [Test]
        [TestCase("svc")]
        [TestCase("-svc")]
        [TestCase("-svc-")]
        [TestCase("--svc")]
        [TestCase("--svc-")]
        [TestCase("app-svc")]
        [TestCase("app-svc-")]
        public void ShouldThrowFormatExServiceInstId(string rohId) {
            Assert.Throws<FormatException>(() => RohIdParser.Parse<RohServiceInstanceId>(rohId));
        }

        [Test]
        [TestCase("svc")]
        [TestCase("--svc")]
        [TestCase("--svc-")]
        [TestCase("app-svc-")]
        public void ShouldThrowFormatExServiceId(string rohId)
        {
            Assert.Throws<FormatException>(() => RohIdParser.Parse<RohServiceId>(rohId));
        }
    }

    [TestFixture]
    public class RohClientTest
    {
        [Test]
        public async Task ShouldReturnServices() {
            var client = new RohClient(Options.Create(new ConsulClientOptions {Address = "http://10.0.10.21:8500"}));
            var kek = await client.Services.All();
        }
    }
}
