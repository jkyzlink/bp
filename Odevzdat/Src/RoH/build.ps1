[CmdletBinding()]
Param(
    [switch]$NoInit,
    [Parameter(Position=0,Mandatory=$false,ValueFromRemainingArguments=$true)]
    [string[]]$BuildArguments
)

Set-StrictMode -Version 2.0; $ErrorActionPreference = "Stop"; $ConfirmPreference = "None"; trap { Write-Host "bum" <#$host.SetShouldExit(1)#> }
$PSScriptRoot = Split-Path $MyInvocation.MyCommand.Path -Parent

###########################################################################
# CONFIGURATION
###########################################################################

$DotNetChannel = "2.0"
$BuildProjectFile = "$PSScriptRoot\.\src\nukeBuild\.build.csproj"
$NuGetVersion = "v4.4.1"

$TempDirectory = "$PSScriptRoot\.tmp"

$NuGetUrl = "https://dist.nuget.org/win-x86-commandline/$NuGetVersion/nuget.exe"
$NuGetFile = "$TempDirectory\nuget.exe"
$env:NUGET_EXE = $NuGetFile

$DotNetScriptUrl = "https://raw.githubusercontent.com/dotnet/cli/master/scripts/obtain/dotnet-install.ps1"
$DotNetDirectory = "$TempDirectory\dotnet-win"
$DotNetFile = $(Get-Command dotnet).Path
$env:DOTNET_EXE = $DotNetFile

$env:DOTNET_SKIP_FIRST_TIME_EXPERIENCE = 1
$env:DOTNET_CLI_TELEMETRY_OPTOUT = 1
$env:NUGET_XMLDOC_MODE = "skip"

###########################################################################
# PREPARE BUILD
###########################################################################

function ExecSafe([scriptblock] $cmd) {
    & $cmd
    if ($LastExitCode -ne 0) { throw "The following call failed with exit code $LastExitCode. '$cmd'" }
}

if (!$NoInit) {
    md -force $DotNetDirectory > $null

	#Download nuget
    if (!(Test-Path $NuGetFile)) { (New-Object System.Net.WebClient).DownloadFile($NuGetUrl, $NuGetFile) }
    elseif ($NuGetVersion -eq "latest") { & $NuGetFile update -Self }
    
    ExecSafe { & $DotNetFile restore $BuildProjectFile -s https://api.nuget.org/v3/index.json }
}

ExecSafe { & $DotNetFile build $BuildProjectFile --no-restore }

###########################################################################
# EXECUTE BUILD
###########################################################################

ExecSafe { & $DotNetFile run --project $BuildProjectFile --no-build -- $BuildArguments }
