﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;

namespace YSoft.Rqa.Deployment.Gui.Test
{
    class Program
    {
        [STAThread]
        static void Main(string[] args) {
            new SelectPackage(new Ruda(new ArtifactoryDownloader("artUrl", "artPath")), null).ShowDialog();
        }
    }
}
