﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System.Collections.Generic;
using System.Linq;
using Squirrel;

namespace YSoft.Rqa.Deployment
{
    /// <summary>
    /// Helps with squirrel's startup events handling.
    /// </summary>
    // ReSharper disable once UnusedMember.Global
    public static class RudaHelper
    {
        private static readonly UpdateManager UpMan = new UpdateManager("don't care");

        /// <summary>
        /// On install: Creates shortcut. On update: updates shortcut.  On uninstall: removes shortcut
        /// 
        /// Application MUST have <code>[assembly: AssemblyMetadata("SquirrelAwareVersion", "1")]</code>
        /// in AssemblyInfo.cs file.
        /// 
        /// It just calls
        /// <code>
        /// SquirrelAwareApp.HandleEvents(
        ///        onInitialInstall: v =&gt; UpMan.CreateShortcutForThisExe(),
        ///        onAppUpdate: v =&gt; UpMan.CreateShortcutForThisExe(),
        ///        onAppUninstall: v =&gt; UpMan.RemoveShortcutForThisExe()
        ///    );
        /// </code>
        /// </summary>
        public static string[] HandleStartup(IEnumerable<string> args) {
            SquirrelAwareApp.HandleEvents(
                onInitialInstall: v => UpMan.CreateShortcutForThisExe(),
                onAppUpdate: v => UpMan.CreateShortcutForThisExe(),
                onAppUninstall: v => UpMan.RemoveShortcutForThisExe()
            );
            var argList = args.ToList();
            argList.RemoveAll(x =>
                x == "--squirrel-install"
                || x == "--squirrel-updated"
                || x == "--squirrel-obsolete"
                || x == "--squirrel-uninstall"
                || x == "--squirrel-firstrun");
            return argList.ToArray();
        }
    }
}