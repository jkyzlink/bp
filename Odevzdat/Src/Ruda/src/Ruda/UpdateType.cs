/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

namespace YSoft.Rqa.Deployment
{
    internal enum UpdateType
    {
        Full,
        Delta
    }
}