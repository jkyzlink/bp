﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using NuGet.Versioning;

namespace YSoft.Rqa.Deployment
{
    /// <summary>
    /// Class for extension methods
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// Retries action
        /// </summary>
        /// <param name="block"></param>
        /// <param name="retries"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T Retry<T>(this Func<T> block, int retries = 2) {
            while (true) {
                try {
                    T ret = block();
                    return ret;
                } catch (Exception) {
                    if (retries == 0) {
                        throw;
                    }

                    retries--;
                    Thread.Sleep(250);
                }
            }
        }

        /// <summary>
        /// Converts semantic version to build date
        /// </summary>
        /// <param name="sv">Semantic version</param>
        /// <returns>Build date</returns>
        public static DateTime? ToPublishDate(this SemanticVersion sv)
        {
            var dateStr = Ruda.MetadataToValuePairs(sv)
                .FirstOrDefault(x => 0 == string.Compare(x.Key, "built", StringComparison.InvariantCultureIgnoreCase))
                .Value;

            if (!DateTime.TryParseExact(dateStr, "yyMMddHHmmss", null, DateTimeStyles.AssumeUniversal, out var date))
                return null;

            return date;
        }

        internal static IEnumerable<TSource> TakeUntil<TSource>(this IEnumerable<TSource> enumerable, Func<TSource, bool> predicate) {
            foreach (var item in enumerable) {
                yield return item;
                if (!predicate(item))
                    break;
            }
        }
    }
}