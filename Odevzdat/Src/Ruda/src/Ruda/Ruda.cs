﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Castle.Core.Logging;
using NuGet.Versioning;
using Squirrel;
using ILogger = Castle.Core.Logging.ILogger;

namespace YSoft.Rqa.Deployment
{
    /// <summary>
    /// RQA Universal Deployment Application
    /// </summary>
    public class Ruda : IUpdater
    {
        private readonly UpdateManager _upMan;
        private readonly IChanneledDownloader _downloader;
        private readonly ILogger _log;

        /// <summary>
        /// RQA Universal Deployment Application
        /// </summary>
        /// <param name="downloader"></param>
        /// <param name="logger"></param>
        public Ruda(IChanneledDownloader downloader, ILogger logger = null) {
            _log = logger ?? new ConsoleLogger();
            _downloader = downloader;
            try {
                _upMan = new UpdateManager("http://nexus.nonexistent/", urlDownloader: _downloader);
            } catch (Exception e) {
                throw new UpdateException($"Unable to instantiate {nameof(UpdateManager)}, application may not be properly installed", e);
            }
            if (!_upMan.IsInstalledApp)
                _log.Warn("Application is NOT properly installed");
            else
                _log.Debug("Application is properly installed");

            try {
                _downloader.SetChannel(CurrentChannel());
            } catch (Exception e) {
                _log.Error($"Unable to set default channel to '{CurrentChannel()}'", e);
            }
        }

        /// <inheritdoc />
        public async Task<bool> NewReleaseAvailableAsync() {
            var updates = await _upMan.CheckForUpdate(false, i => { }).ConfigureAwait(false);
            return updates.ReleasesToApply?.Any() ?? false;
        }

        /// <inheritdoc />
        public async Task<IOrderedEnumerable<AppVersion>> PossibleReleasesAsync() {
            var releases = await _upMan.PossibleUpdates().ConfigureAwait(false);
            return releases.Select(x => new AppVersion {
                    Version = x.Version,
                    BuildDate = x.Version.ToPublishDate()
                })
                .OrderBy(x => x.Version);
        }

        /// <inheritdoc />
        public async Task UpdateToReleaseAsync(AppVersion desiredAppVersion, bool restart) {
            _log.Info($"Updating to version {desiredAppVersion.Version}, built {desiredAppVersion.BuildDate}");
            _log.Debug("Downloading available releases");
            var releases = (await _upMan.PossibleUpdates().ConfigureAwait(false)).OrderBy(x => x.Version).ToList();
            _log.Debug("Searching for desired release");
            var desiredRelease = releases.LastOrDefault(x => x.Version == desiredAppVersion.Version);
            if (desiredRelease == null) {
                _log.Error("Desired release was not found, update has been canceled");
                return;
            }
            if (desiredRelease.Version == _upMan.CurrentlyInstalledVersion()) {
                _log.Info($"Currently installed version is same as the selected one {desiredRelease.Version}=={_upMan.CurrentlyInstalledVersion()}, skipping update...");
                return;
            }
            var releasesToApply = releases.TakeUntil(x => x.Version != desiredAppVersion.Version).ToList();
            var updateInfo = _upMan.DetermineUpdateInfo(releasesToApply);
            _log.Debug("Update info determined, downloading new release");
            try {
                await _upMan.DownloadReleases(new List<ReleaseEntry> {updateInfo.FutureReleaseEntry}).ConfigureAwait(false);
                _log.Info("New releases has been downloaded, updating...");
                var appVer = await _upMan.ApplyReleases(updateInfo, null, updateInfo.FutureReleaseEntry.Version).ConfigureAwait(false);
                _log.Info($"Application has been updated to {appVer}");
                if (restart)
                    RestartApplication();
            } catch (Exception e) {
                _log.Error("Unable to update application", e);
                throw;
            }
        }

        private void RestartApplication() {
            _log.Info("Killing other instances of the application");
            _upMan.KillAllExecutablesBelongingToPackage();
            _log.Debug("Restarting application");
            _upMan.Dispose();

            ExitAndRunSquirrelAwareApp();
        }

        private void ExitAndRunSquirrelAwareApp() {
            //Update manager is squirrel aware but we need to prefer other executables
            if (!Assembly.GetEntryAssembly().FullName.StartsWith("Ruda.UpdateShortcut"))
                //Entry assembly is squirrel aware so just restart it
                if (Assembly.GetEntryAssembly().GetCustomAttributes<AssemblyMetadataAttribute>().Any(x => x.Key == "SquirrelAwareVersion")) {
                    UpdateManager.RestartApp();
                }

            //Current entry assembly is not squirrel aware e.g. Update manager
            //Try to find squirrel aware executable and start it after restart
            var possiblePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            _log.Debug($"Looking for squirrel aware executables in \"{possiblePath}\"");

            var squirrelAwareApps = MySquirrelAwareExecutableDetector.GetAllSquirrelAwareApps(possiblePath);
            //Remove Ruda update manager from the list
            squirrelAwareApps.Remove(Assembly.GetEntryAssembly().Location);

            if (squirrelAwareApps.Count > 1) {
                _log.Error($"Found more than one squirrel aware executable ({string.Join(", ", squirrelAwareApps)})");
                UpdateManager.RestartApp();
                return;
            }

            if (squirrelAwareApps.Count == 0) {
                _log.Info("Not found any squirrel aware executable.");
                UpdateManager.RestartApp();
                return;
            }
            _log.Debug($"Found single squirrel aware executable {squirrelAwareApps.First()}");

            var executableName = Path.GetFileName(squirrelAwareApps.First());
            UpdateManager.RestartApp(executableName);
        }

        /// <inheritdoc />
        public async Task UpdateToLatestAsync(bool restart) {
            try {
                _log.Info("Updating to latest version");
                _log.Debug("Downloading available releases");
                var lastRelease = (await PossibleReleasesAsync().ConfigureAwait(false)).LastOrDefault();
                if (lastRelease == null) {
                    _log.Info("Already up-to-date");
                    return;
                }
                await UpdateToReleaseAsync(lastRelease, restart).ConfigureAwait(false);
            } catch (Exception e) {
                _log.Error("Unable to update application", e);
                throw;
            }
        }

        /// <inheritdoc />
        public AppVersion CurrentVersion() {
            var semantic = TryGetCurrentVersion();
            if (semantic == null)
                return null;
            return new AppVersion {Version = semantic, BuildDate = semantic.ToPublishDate()};
        }

        /// <inheritdoc />
        public string CurrentChannel() {
            var version = TryGetCurrentVersion();
            if (version == null)
                return null;
            return CurrentChannelImpl(version);
        }

        /// <summary>
        /// Parses channel from <see cref="SemanticVersion"/>. If metadata contains "branch.{branchName}"
        /// returns branchName, otherwise master if prerelease or <see cref="string.Empty"/> if not prerelease.
        /// </summary>
        /// <param name="semVer">Semantic version</param>
        /// <returns>Channel name</returns>
        public static string CurrentChannelImpl(SemanticVersion semVer) {
            if (semVer == null)
                throw new ArgumentNullException(nameof(semVer));

            var metaBranch = MetadataToValuePairs(semVer)
                .FirstOrDefault(x => 0 == string.Compare(x.Key, "branch", StringComparison.InvariantCultureIgnoreCase))
                .Value;

            if (!semVer.IsPrerelease && (metaBranch == null || metaBranch == "master"))
                return "master";

            return semVer.HasMetadata ? metaBranch : string.Empty;
        }

        /// <summary>
        /// Get metadata as key-value pairs
        /// </summary>
        /// <param name="semVer">Semantic version</param>
        /// <returns></returns>
        public static IDictionary<string, string> MetadataToValuePairs(SemanticVersion semVer)
        {
            var ret = new Dictionary<string, string>();
            var parts = semVer.Metadata.Split('.');
            for (var i = 0; i < parts.Length - 1; i += 2)
                ret.Add(parts[i], parts[i + 1]);

            return ret;
        }

        /// <inheritdoc />
        public async Task<List<string>> AvailableChannelsAsync() => await _downloader.GetAvailableChannelsAsync().ConfigureAwait(false);

        /// <inheritdoc />
        public string FutureChannel() => _downloader.UpdateChannel;

        /// <inheritdoc />
        public async Task SetFutureChanelAsync(string channel) => await _downloader.SwitchChannelAsync(channel).ConfigureAwait(false);

        private SemanticVersion TryGetCurrentVersion() {
            try {
                return _upMan.CurrentlyInstalledVersion();
            } catch (Exception e) {
                _log.Warn("Error determining currently installed version", e);
            }
            return null;
        }

        /// <inheritdoc />
        public void Dispose() {
            _upMan?.Dispose();
        }
    }
}