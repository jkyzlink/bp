﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using NuGet.Versioning;

namespace YSoft.Rqa.Deployment
{

    /// <summary>
    /// Contains application version and build date
    /// </summary>
    public class AppVersion
    {
        /// <summary>
        /// When was the application build
        /// </summary>
        public DateTime? BuildDate { get; set; }

        /// <summary>
        /// <see cref="SemanticVersion"/> of the application
        /// </summary>
        public SemanticVersion Version { get; set; }

        /// <inheritdoc />
        public override string ToString() => $"{Version}, build: {BuildDate}";
    }
}