/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YSoft.Rqa.Deployment
{
    /// <summary>
    /// Application updater
    /// </summary>
    public interface IUpdater : IDisposable
    {
        /// <summary>
        /// Determine whether there are new release of the application available
        /// </summary>
        /// <returns>True if so, otherwise false</returns>
        Task<bool> NewReleaseAvailableAsync();

        /// <summary>
        /// Get all possible down/updates on current channel
        /// </summary>
        /// <returns>List of possible releases</returns>
        Task<IOrderedEnumerable<AppVersion>> PossibleReleasesAsync();

        /// <summary>
        /// Updates application to the specific version.
        /// So, it downloads and applies release. 
        /// Optionally kills all other instances of this application
        /// and restarts current instance.
        /// </summary>
        /// <param name="desiredAppVersion">Target version</param>
        /// <param name="restart">Whether to restart application after update</param>
        Task UpdateToReleaseAsync(AppVersion desiredAppVersion, bool restart = true);

        /// <summary>
        /// Finds if there is new version on currently selected channel, if so
        /// updates application to the latest version,
        /// and optionally kills all other instances of the application
        /// and restarts this instance of application.
        /// </summary>
        /// <param name="restart">Whether to restart application after update</param>
        Task UpdateToLatestAsync(bool restart = true);

        /// <summary>
        /// Gets current application version
        /// </summary>
        /// <returns>Application version</returns>
        AppVersion CurrentVersion();

        /// <summary>
        /// Gets currently used channel
        /// </summary>
        /// <returns>Current channel</returns>
        string CurrentChannel();

        /// <summary>
        /// Gets collection of the available channels
        /// </summary>
        /// <returns>Available channels</returns>
        Task<List<string>> AvailableChannelsAsync();

        /// <summary>
        /// Channel, used for future update
        /// </summary>
        /// <returns>Future channel</returns>
        string FutureChannel();

        /// <summary>
        /// Sets future channel <see cref="Ruda.FutureChannel"/>
        /// </summary>
        /// <param name="channel">Future channel name</param>
        Task SetFutureChanelAsync(string channel);
    }
}