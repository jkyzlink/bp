/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Squirrel;

namespace YSoft.Rqa.Deployment
{
    /// <summary>
    /// Downloader, that supports channeling
    /// </summary>
    public interface IChanneledDownloader : IFileDownloader, IDisposable
    {
        /// <summary>
        /// Verifies, whether the specified channel exists and whether is valid
        /// </summary>
        /// <param name="channel">Channel</param>
        /// <returns>True if exists, otherwise false</returns>
        Task<bool> VerifyChannelAsync(string channel);

        /// <summary>
        /// Gets list of available channels
        /// </summary>
        /// <returns>Available channels</returns>
        Task<List<string>> GetAvailableChannelsAsync();

        /// <summary>
        /// Sets channel without verifying
        /// </summary>
        /// <param name="channel">Channel</param>
        void SetChannel(string channel);

        /// <summary>
        /// Verifies, if channel exists if so sets it
        /// </summary>
        /// <param name="channel">Channel</param>
        Task SwitchChannelAsync(string channel);

        /// <summary>
        /// Current update channel
        /// </summary>
        string UpdateChannel { get; }
    }
}