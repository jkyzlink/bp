/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Text.RegularExpressions;
using NuGet.Versioning;

namespace YSoft.Rqa.Deployment
{
    internal class UpdateFile
    {
        internal readonly UpdateType Type;
        internal readonly SemanticVersion Version;
        internal readonly string Extension;
        internal readonly string Name;

        private readonly Regex _versionRegex = new Regex(@"^(?'File'.+?)\-(?'Version'.+)\-(?'Type'full|delta)\.(?'Extension'\w+)$", RegexOptions.Compiled);

        internal UpdateFile(string fileName) {
            if (string.IsNullOrWhiteSpace(fileName))
                throw new ArgumentNullException(nameof(fileName));

            var match = _versionRegex.Match(fileName);
            if(!match.Success)
                throw new UpdateException($"Unable to parse file name {fileName}");

            if (!Enum.TryParse(match.Groups["Type"].Value, true, out Type))
                throw new ArgumentException("Cannot parse update type", nameof(fileName));

            Name = match.Groups["File"].Value;
            Version = SemanticVersion.Parse(match.Groups["Version"].Value);
            Extension = match.Groups["Extension"].Value;
        }
    }
}