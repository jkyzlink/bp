﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NuGet.Versioning;
using ILogger = Castle.Core.Logging.ILogger;
using NullLogger = Castle.Core.Logging.NullLogger;

namespace YSoft.Rqa.Deployment
{
    /// <summary>
    /// Null updater, does nothing
    /// </summary>
    // ReSharper disable once ClassNeverInstantiated.Global
    public class NullUpdater : IUpdater
    {
        /// <summary>
        /// Just write to
        /// </summary>
        /// <param name="log"></param>
        public NullUpdater(ILogger log) {
            log = log ?? NullLogger.Instance;
            log.Info($"Using {nameof(NullUpdater)}");
        }

        /// <summary>
        /// Does nothing
        /// </summary>
        /// <returns>false</returns>
        public Task<bool> NewReleaseAvailableAsync() {
            return Task.FromResult(false);
        }

        /// <summary>
        /// Does nothing
        /// </summary>
        /// <returns>Empty enumerable</returns>
        public Task<IOrderedEnumerable<AppVersion>> PossibleReleasesAsync() {
            return Task.FromResult(Enumerable.Empty<AppVersion>().OrderBy(x => x.Version));
        }

        /// <summary>
        /// Does nothing
        /// </summary>
        public Task UpdateToReleaseAsync(AppVersion desiredAppVersion, bool restart = true) {
            return Task.FromResult(typeof(void));
        }

        /// <summary>
        /// Does nothing
        /// </summary>
        public Task UpdateToLatestAsync(bool restart) {
            return Task.FromResult(typeof(void));
        }

        /// <summary>
        /// Does nothing
        /// </summary>
        /// <returns>Version 1.0</returns>
        public AppVersion CurrentVersion() {
            return new AppVersion {BuildDate = DateTime.UtcNow, Version = new SemanticVersion(1, 0, 0)};
        }

        /// <summary>
        /// Does nothing
        /// </summary>
        /// <returns>nullUploader</returns>
        public string CurrentChannel() {
            return "nullUploader";
        }

        /// <summary>
        /// Does nothing
        /// </summary>
        /// <returns>Empty list</returns>
        public Task<List<string>> AvailableChannelsAsync() {
            return Task.FromResult(new List<string>());
        }

        /// <summary>
        /// Does nothing
        /// </summary>
        /// <returns>nullUploader</returns>
        public string FutureChannel() {
            return CurrentChannel();
        }

        /// <summary>
        /// Does nothing
        /// </summary>
        public Task SetFutureChanelAsync(string channel) {
            return Task.FromResult(typeof(void));
        }

        /// <inheritdoc />
        public void Dispose() {
            //nothing to dispose
        }
    }
}