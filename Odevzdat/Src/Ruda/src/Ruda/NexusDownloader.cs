﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using Flurl;
using Flurl.Http;
using ILogger = Castle.Core.Logging.ILogger;
using NullLogger = Castle.Core.Logging.NullLogger;

namespace YSoft.Rqa.Deployment
{
    /// <summary>
    /// Downloads releases from sonatype's nexus
    /// </summary>
    public class NexusDownloader : IChanneledDownloader
    {
        /// <inheritdoc />
        public string UpdateChannel { get; private set; } = "master";

        private readonly string _nexusBaseUrl;
        private readonly string _groupId;
        private readonly string _artifactId;
        private readonly ILogger _log;
        private FlurlClient FClient => _fClient ?? (_fClient = new FlurlClient());
        private FlurlClient _fClient;
        private string NexusMavenService => _nexusBaseUrl.AppendPathSegments("service/local/artifact/maven/content");
        private string NexusContentService => _nexusBaseUrl.AppendPathSegments("content");

        /// <summary>
        /// Downloader that download artifacts from sonatype's nexus repository
        /// </summary>
        /// <param name="nexusBaseUrl">Nexus's base url </param>
        /// <param name="groupId">Application's artifact's group id used in nexus</param>
        /// <param name="artifactId">Application's artifact id </param>
        /// <param name="logger">Optionally castle <see cref="ILogger"/></param>
        public NexusDownloader(string nexusBaseUrl, string groupId, string artifactId, ILogger logger = null) {
            _log = logger ?? NullLogger.Instance;
            _nexusBaseUrl = nexusBaseUrl;
            _groupId = groupId;
            _artifactId = artifactId;
            _log.Debug($"Instantiated new {nameof(NexusDownloader)}. Group: {groupId}, Artifact: {artifactId}, Nexus: {nexusBaseUrl}");
        }

        /// <summary>
        /// Squirrel use it
        /// </summary>
        /// <param name="url"></param>
        /// <param name="targetFile"></param>
        /// <param name="progress"></param>
        /// <returns></returns>
        public async Task DownloadFile(string url, string targetFile, Action<int> progress) {
            _log.Debug($"Downloading file: {targetFile} from {url}");
            await DownloadPackageInternalAsync(targetFile, progress).ConfigureAwait(false);
        }

        private async Task DownloadPackageInternalAsync(string targetFile, Action<int> progress) {
            var update = new UpdateFile(System.IO.Path.GetFileName(targetFile));
            var directory = System.IO.Path.GetDirectoryName(targetFile);

            const string repoName = "snapshots";
            string artifactId = $"{_artifactId}.{(update.Extension == "nupkg" ? "packages" : "deltas")}";

            _log.Debug($"Downloading file {update.Name + update.Extension}, to update app to version {update.Version} from {NexusMavenService}");

            var fileUrl = NexusMavenService
                .SetQueryParam("r", repoName)
                .SetQueryParam("g", _groupId)
                .SetQueryParam("a", artifactId)
                .SetQueryParam("v", $"2-{UpdateChannel}-{update.Version.ToFullString()}-SNAPSHOT")
                .SetQueryParam("e", update.Type != UpdateType.Delta ? update.Extension : update.Extension + "d");

            _log.Debug($"file url {fileUrl}");

            await fileUrl
                .WithClient(FClient)
                .DownloadFileAsync(directory, targetFile)
                .ConfigureAwait(false);
        }

        /// <inheritdoc />
        public async Task SwitchChannelAsync(string channel) {
            if (!await VerifyChannelAsync(channel).ConfigureAwait(false)) {
                _log.Warn($"{channel} is invalid switch does not proceed");
                throw new UpdateException($"Invalid channel '{channel}'");
            }
            UpdateChannel = channel;
            _log.Info($"Switching channel to {UpdateChannel}");
        }

        /// <inheritdoc />
        public void SetChannel(string channel) {
            UpdateChannel = channel;
            _log.Info($"Update channel has been set to {channel}");
        }

        /// <summary>
        /// Squirrel use it
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public Task<byte[]> DownloadUrl(string url) {
            var targetFile = new Uri(url).Segments.Last();
            return DownloadFileAsync(targetFile, UpdateChannel);
        }

        private Task<byte[]> DownloadFileAsync(string targetFile, string channel) {
            if (targetFile == "RELEASES") {
                const string repoName = "snapshots";
                string version = $"2-{channel}-SNAPSHOT";
                const string extension = "txt";

                return NexusMavenService
                    .SetQueryParams(new {
                        r = repoName,
                        g = _groupId,
                        a = $"{_artifactId}.{targetFile}",
                        v = version,
                        e = extension
                    })
                    .WithClient(FClient)
                    .GetBytesAsync();
            }
            return null;
        }

        /// <inheritdoc />
        public async Task<bool> VerifyChannelAsync(string channel) {
            var channels = await GetAvailableChannelsAsync().ConfigureAwait(false);
            if (!channels.Contains(channel))
                return false;

            try {
                var releasesFileBytes = await DownloadFileAsync("RELEASES", channel).ConfigureAwait(false);
                //Just to verify that releases file is there
                var releases = Encoding.UTF8.GetString(releasesFileBytes);
            } catch {
                return false;
            }
            return true;
        }

        /// <inheritdoc />
        public async Task<List<string>> GetAvailableChannelsAsync() {
            var slashedGroupId = _groupId.Replace('.', '/');
            //Get maven metadata (maven-metadata.xml)
            var metadata = await NexusContentService
                               .AppendPathSegments("repositories", "snapshots", slashedGroupId, $"{_artifactId}.RELEASES", "maven-metadata.xml")
                               .WithClient(FClient)
                               .GetStringAsync()
                               .ConfigureAwait(false);
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(metadata);

            var versionRegex = new Regex(@"(?:2-)(?'version'(?:\w{2,6}-\d+|master|dev(?:elop)?))(?:-\d+\.\d+\.\d+)?(?:-SNAPSHOT)", RegexOptions.IgnoreCase);

            var versionsNode = xmlDoc.GetElementsByTagName("version");
            var mathes = versionsNode
                .Cast<XmlNode>()
                .Select(node => node.InnerText)
                .Select(x => versionRegex.Match(x).Groups["version"]);

            return mathes.Select(x => x.ToString()).ToList();
        }

        /// <inheritdoc />
        public void Dispose() {
            _fClient?.Dispose();
        }
    }
}