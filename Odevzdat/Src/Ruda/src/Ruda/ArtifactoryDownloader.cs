﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Core.Logging;
using Flurl;
using Flurl.Http;
using HtmlAgilityPack;

namespace YSoft.Rqa.Deployment
{
    /// <inheritdoc />
    public class ArtifactoryDownloader : IChanneledDownloader
    {
        /// <inheritdoc />
        public string UpdateChannel { get; private set; } = "master";

        private readonly string _baseUrl;
        private readonly string _projectPath;
        private string BaseDownloadUrl => _baseUrl.AppendPathSegment(_projectPath);
        private string ChannelUrlSegment(string channel = null) => channel ?? UpdateChannel;
        private FlurlClient _fClient;
        private FlurlClient FClient => _fClient ?? (_fClient = new FlurlClient());
        private readonly ILogger _log;

        /// <summary>
        /// Instantiates new JFrog's Artifactory downloader.
        /// </summary>
        /// <param name="baseUrl">Artifactory nuget repository base url ex. <![CDATA[https://artifactory/Nuget_Internal/]]></param>
        /// <param name="projectPath">Project path with repository name</param>
        /// <param name="log"><see cref="ILogger"/> optional logger</param>
        public ArtifactoryDownloader(string baseUrl, string projectPath, ILogger log = null) {
            _baseUrl = baseUrl;
            _projectPath = projectPath;
            _log = log ?? NullLogger.Instance;
            _log.Info($"New {nameof(ArtifactoryDownloader)} - {nameof(baseUrl)}:\'{baseUrl}\' {nameof(projectPath)}: \'{projectPath}\'");
        }

        /// <inheritdoc />
        public async Task DownloadFile(string url, string targetFile, Action<int> progress) {
            _log.Debug($"Downloading file: \'{targetFile}\' from \'{url}\'");

            var update = new UpdateFile(System.IO.Path.GetFileName(targetFile));
            var fileUrl = BaseDownloadUrl.AppendPathSegments(ChannelUrlSegment(), System.IO.Path.GetFileName(targetFile));
            _log.Debug($"Downloading file \'{update.Name + update.Extension}\', from \'{fileUrl}\', version \'{update.Version.ToFullString()}\'");

            await fileUrl
                .WithClient(FClient)
                .DownloadFileAsync(System.IO.Path.GetDirectoryName(targetFile), targetFile)
                .ConfigureAwait(false);
        }

        /// <inheritdoc />
        public Task<byte[]> DownloadUrl(string url) {
            _log.Debug($"Downloading url: \'{url}\'");
            var targetFile = new Uri(url).Segments.Last();
            return DownloadFileAsync(targetFile, UpdateChannel);
        }

        /// <inheritdoc />
        public void Dispose() {
            _fClient?.Dispose();
            _fClient = null;
        }

        /// <inheritdoc />
        public async Task<bool> VerifyChannelAsync(string channel) {
                var channels = await GetAvailableChannelsAsync().ConfigureAwait(false);
                if (!channels.Contains(channel))
                    return false;

            try {
                var releasesFileBytes = await DownloadFileAsync("RELEASES", channel).ConfigureAwait(false);
                //Just to verify that releases file is there
                var releases = Encoding.UTF8.GetString(releasesFileBytes);
            } catch (Exception) {
                return false;
            }

            return true;
        }

        private Task<byte[]> DownloadFileAsync(string targetFile, string channel) {
            _log.Debug($"Downloading file \'{targetFile}\'");
            if (targetFile == "RELEASES") {
                return BaseDownloadUrl
                    .AppendPathSegments(ChannelUrlSegment(channel), targetFile)
                    .WithClient(FClient)
                    .GetBytesAsync();
            }

            return null;
        }

        /// <inheritdoc />
        public async Task<List<string>> GetAvailableChannelsAsync() {
            var ret = new List<string>();
            var doc = new HtmlDocument();
            try {
                var channelsPageStream = await BaseDownloadUrl
                                                .WithClient(FClient)
                                                .GetStreamAsync()
                                                .ConfigureAwait(false);

                doc.Load(channelsPageStream);
            } catch (Exception) {
                _log.Info("Unable to download channel list");
                return ret;
            }

            var nodes = doc.DocumentNode.SelectNodes(@"/html/body/pre[2]/a").Nodes();

            if (nodes == null)
                return ret;

            ret.AddRange(
                nodes
                    .Select(node => node.InnerText.TrimEnd('/'))
                    .Where(channel => channel != "..")
            );

            return ret;
        }

        /// <inheritdoc />
        public void SetChannel(string channel) {
            UpdateChannel = channel;
            _log.Info($"Update channel has been set to \'{channel}\'");
        }

        /// <inheritdoc />
        public async Task SwitchChannelAsync(string channel) {
            if (!await VerifyChannelAsync(channel).ConfigureAwait(false)) {
                _log.Warn($"Channel '{channel}' is invalid, switch can not proceed.");
                throw new UpdateException($"Invalid channel \'{channel}\'");
            }

            UpdateChannel = channel;
            _log.Info($"Switching channel to \'{UpdateChannel}\'");
        }
    }
}