/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;

namespace YSoft.Rqa.Deployment
{
    class UpdateException : Exception
    {
        public UpdateException() {}

        public UpdateException(string message) : base(message) {}

        public UpdateException(string message, Exception innerException) : base(message, innerException) {}
    }
}