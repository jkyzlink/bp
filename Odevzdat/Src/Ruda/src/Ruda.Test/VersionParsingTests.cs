﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Globalization;
using NuGet.Versioning;
using NUnit.Framework;

namespace YSoft.Rqa.Deployment.Test {
    public class VersionParsingTests
    {
        [TestCase("master", "1.2.3")]
        [TestCase("master", "1.2.3+branch.master")]
        [TestCase("master", "1.2.3-test+branch.master")]
        [TestCase("devel", "1.2.3+branch.devel")]
        [TestCase("devel", "1.2.3-test+branch.devel")]
        [TestCase("devel", "1.2.3-test+sha.123456.branch.devel")]
        [TestCase("devel", "1.2.3-test+sha.123456.branch.devel.some.thing")]
        public void ShouldParseChannels(string channel, string semVerStr) {
            var semVer = SemanticVersion.Parse(semVerStr);
            var ch = Ruda.CurrentChannelImpl(semVer);
            StringAssert.AreEqualIgnoringCase(channel, ch);
        }

        [TestCase(null, "1.2.3")]
        [TestCase(null, "1.2.3-beta")]
        [TestCase(null, "1.2.3+built.18040312304")]
        [TestCase(null, "1.2.3-beta+built.18040312304")]
        [TestCase(null, "1.2.3-beta+sha.badcaffe")]
        [TestCase("180403123040", "1.2.3+built.180403123040")]
        [TestCase("180403123040", "1.2.3-beta+built.180403123040")]
        [TestCase("180403123040", "1.2.3-beta+sha.badcaffe.built.180403123040")]
        [TestCase("180403123040", "1.2.3-beta+built.180403123040.sha.badcaffe")]
        public void ShouldParseBuildDate(string correctDateStr, string semVerStr)
        {
            var date = SemanticVersion.Parse(semVerStr).ToPublishDate();
            DateTime? correctDate = null;
            if (correctDateStr != null)
             correctDate = DateTime.ParseExact(correctDateStr, "yyMMddHHmmss", null, DateTimeStyles.AssumeUniversal);
            Assert.That(date, Is.EqualTo(correctDate));
        }
    }
}