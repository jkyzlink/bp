﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System.Collections.Generic;
using System.Threading.Tasks;
using NUnit.Framework;
using YSoft.Rqa.Deployment.Cli;
using YSoft.Rqa.Deployment.Gui;

namespace YSoft.Rqa.Deployment.Test
{
    public class CliTests
    {
        [Test]
        public async Task Should_ShowAllInfo()
        {
            var cli = new RudaCli(new NullUpdater(null), null, null);
            cli.Parse(new[] { "info", "-vcl" });
            await cli.ExecuteAsync().ConfigureAwait(false);
        }

        [Test]
        public void Should_ShowHelpRoot()
        {
            var cli = new RudaCli(new NullUpdater(null), null, null);
            cli.Parse(new[] { "--help" });
        }

        [Test]
        public void Should_ShowHelpUpdate()
        {
            var cli = new RudaCli(new NullUpdater(null), null, null);
            cli.Parse(new[] { "--help", "update" });
        }

        [Test]
        public void Should_ShowHelpInfo()
        {
            var cli = new RudaCli(new NullUpdater(null), null, null);
            cli.Parse(new[] { "--help", "info" });
        }

        [Test]
        public async Task Should_ShowGui()
        {
            var cli = new RudaCli(new NullUpdater(null), new List<ICliExtension> {new GuiCliExtension(null)},  null);
            cli.Parse(new[] { "gui"});
            await cli.ExecuteAsync().ConfigureAwait(false);
        }
    }
}