﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System.Threading.Tasks;
using NUnit.Framework;

namespace YSoft.Rqa.Deployment.Test
{
    public class NexusDownloaderTests
    {
        private const string NexusUrl = "nexusUrl";
        private const string GroupId = "nexus.group";
        private const string ArtifactId = "YSoftRobot";

        private NexusDownloader _dl;

        [OneTimeSetUp]
        public void DownloaderCtor() {
            _dl = new NexusDownloader(NexusUrl, GroupId, ArtifactId);
        }

        [TestCase("master", ExpectedResult = true)]
        [TestCase("dev", ExpectedResult = true)]
        [TestCase("InvalidFooBar", ExpectedResult = false)]
        public bool ShouldTestChannelDuringConstruction(string channel)
        {
            try {
                _dl = new NexusDownloader(NexusUrl, GroupId, ArtifactId);
            } catch {
                return false;
            }
            return true;
        }

        [TestCase("master", ExpectedResult = true)]
        [TestCase("dev", ExpectedResult = true)]
        [TestCase("InvalidFooBar", ExpectedResult = false)]
        public async Task<bool> ShouldVerifyChannel(string channel) {
            return await _dl.VerifyChannelAsync(channel);
        }

        [Test]
        public async Task ShouldGetAvailableChannels() {
            var channels = await _dl.GetAvailableChannelsAsync();
            Assert.Contains("dev", channels);
        }
    }
}
