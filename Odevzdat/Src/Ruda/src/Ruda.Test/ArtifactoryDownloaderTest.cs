﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;

namespace YSoft.Rqa.Deployment.Test
{
    public class ArtifactoryDownloaderTest
    {
        private const string ArtifactoryUrl = "artUrl";
        private const string ProjectPath = "artPath";

        private ArtifactoryDownloader _dl;

        [OneTimeSetUp]
        public void Ctor() {
            _dl = new ArtifactoryDownloader(ArtifactoryUrl, ProjectPath);
        }


        [Test]
        public async Task ShouldGetAvailableChannelsAndVerify() {
            var channels = await _dl.GetAvailableChannelsAsync();
            Assert.That(channels, Is.Not.Empty);
            Assert.That(channels, Is.Not.Contain(".."));
            var chExists = await _dl.VerifyChannelAsync(channels.First());
            Assert.That(chExists, Is.True);
        }
    }
}