﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using Nuke.Common.IO;

namespace YSoft.Rqa.Deployment.Nuke
{
    public class RudaSettings
    {
        public string SourceDirectory { get; set; }
        internal readonly Lazy<PathConstruction.AbsolutePath> SourceDir;
        public string OutputPath { get; set; }
        internal readonly Lazy<PathConstruction.AbsolutePath> OutDir;
        public string ArtifactoryUrl { get; set; }
        public string ArtifactoryProjectPath { get; set; }
        public string ArtifactoryApiKey { get; set; }
        public string BranchId { get; set; }
        public Func<string, string> BranchToId { get; set; }
        public bool CreateMsi { get; set; } = false;
        public int PreserveReleases { get; set; } = 10;

        public RudaSettings() {
            SourceDir = new Lazy<PathConstruction.AbsolutePath>(() => {
                FileSystemTasks.EnsureExistingDirectory(SourceDirectory);
                return (PathConstruction.AbsolutePath) SourceDirectory;
            });
            OutDir = new Lazy<PathConstruction.AbsolutePath>(() => {
                FileSystemTasks.EnsureExistingDirectory(OutputPath);
                return (PathConstruction.AbsolutePath) OutputPath;
            });
        }
    }
}
