﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using Flurl.Http.Content;
using LibGit2Sharp;
using NuGet.Packaging;
using NuGet.Versioning;
using Nuke.Common;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.NuGet;
using Rqa.NukeHelpers;
using Squirrel;
using YSoft.Rqa.Deployment;
using YSoft.Rqa.Deployment.Nuke;
using static Nuke.Common.IO.PathConstruction;
using static Nuke.Common.IO.FileSystemTasks;

namespace Ruda.Nuke
{
    /// <summary>
    /// Tasks for easier ruda use
    /// </summary>
    public class RudaTasks
    {
        /// <summary>
        /// Creates NuGet packages based on appspec files
        /// </summary>
        /// <param name="configurator">Ruda configuration functor</param>
        public static void CreateNugetPackage(Func<RudaSettings> configurator)
        {
            var settings = configurator();
            var appSpecs = GlobFiles(settings.SourceDir.Value, "*.appspec");
            foreach (var appSpec in appSpecs)
            {
                var nuSpec = (AbsolutePath)Path.GetDirectoryName(appSpec) / $"{Path.GetFileNameWithoutExtension(appSpec)}.nuspec";
                if (File.Exists(nuSpec))
                {
                    Logger.Trace($"Found previously generated nuspec, deleting '{nuSpec}'");
                    File.Delete(nuSpec);
                }

                Logger.Trace($"Creating a new nuspec '{nuSpec}' from '{appSpec}'");
                File.Copy(appSpec, nuSpec);

                var appPath = settings.OutDir.Value / new NuspecReader(nuSpec).GetIdentity().Id;
                Logger.Trace($"Creating a nuget package from '{nuSpec}' and saving it to the directory '{appPath}'.");
                NuGetTasks.NuGetPack(cfg => NuGetTasks.DefaultNuGetPack
                    .SetTargetPath(nuSpec)
                    .SetVersion(VersionCreation.Version.ToFullString())
                    .SetOutputDirectory(appPath)
                    .SetBasePath(Path.GetDirectoryName(nuSpec))
                    .EnableForceEnglishOutput()
                );
                Logger.Info($"Created nuget package from '{Path.GetFileName(nuSpec)}'");
            }
        }

        /// <summary>
        /// Downloads old RudaPackages for specified branch to ensure proper delta packages creation
        /// </summary>
        /// <param name="configurator">Ruda configuration functor</param>
        public static async Task DownloadOldPackages(Func<RudaSettings> configurator)
        {
            var settings = configurator();
            foreach (AbsolutePath appDir in Directory.GetDirectories(settings.OutDir.Value))
            {
                //In fact it gets folder name
                var appName = Path.GetFileName(appDir);
                var releasesDir = appDir / "Releases";
                EnsureExistingDirectory(releasesDir);
                var releasePath = releasesDir / "RELEASES";
                var releases = await GetReleaseEntries(appName, settings, settings.BranchId, releasePath, true);
                if (releases.entries == null)
                {
                    Logger.Trace($"Old packages for '{appName}' does not exists, skipping download");
                    continue;
                }
                var lastRelease = releases.entries.OrderBy(x => x.Version).LastOrDefault()?.Filename;
                Logger.Info($"Last release version: '{lastRelease}'");
                Logger.Info($"Downloading '{lastRelease}' to '{releasesDir / lastRelease}'");
                await releases.downloader.DownloadFile(string.Empty, releasesDir / lastRelease, null);
                Logger.Info($"Downloaded '{lastRelease}' to '{releasesDir / lastRelease}'");
            }
        }

        static async Task<(ArtifactoryDownloader downloader, IQueryable<ReleaseEntry> entries)> GetReleaseEntries(
            string appName, RudaSettings settings, string channel, string releasesPath = null, bool saveReleases = false)
        {
            Logger.Trace($"Downloading old releases for the '{appName}', channel: '{channel}'");
            var dm = new ArtifactoryDownloader(settings.ArtifactoryUrl, settings.ArtifactoryProjectPath.AppendPathSegment(appName));
            try
            {
                if (!await dm.VerifyChannelAsync(channel))
                {
                    const string msg = "Channel does not exist on a remote server";
                    Logger.Info(msg);
                    return (dm, null);
                }
            }
            catch (Exception e)
            {
                Logger.Info($"Error verifying channels {e}{Environment.NewLine}{e.StackTrace}");
                throw new Exception("Error verifying channels");
            }

            dm.SetChannel(channel);
            Logger.Info($"Downloading the RELASES file for channel '{channel}'");
            var bytes = await dm.DownloadUrl("http://foo.bar/RELEASES");
            if (saveReleases)
            {
                File.WriteAllBytes(releasesPath, bytes);
                Logger.Info($"RELEASES downloaded and saved to '{releasesPath}'");
            }

            var entries = ReleaseEntry.ParseReleaseFile(Encoding.UTF8.GetString(bytes)).AsQueryable();
            Logger.Trace($"Found {entries.Count()} entries in the RELEASE file");
            return (dm, entries);
        }

        /// <summary>
        /// Creates RudaPackage (and setup) and optionally MSI installer
        /// </summary>
        /// <param name="configurator">Ruda configuration functor</param>
        public static void CreateRudaPackage(Func<RudaSettings> configurator)
        {
            var settings = configurator();
            var squirrelExe = LocateSquirrel();

            foreach (var appDir in Directory.GetDirectories(settings.OutDir.Value))
            {
                Logger.Trace($"Processing directory '{appDir}'");
                foreach (var package in Directory.GetFiles(appDir).Where(x => x.EndsWith(".nupkg")))
                {
                    Logger.Info($"Invoking Squirrel's releasify for '{package}'");
                    var relProcess = ProcessTasks.StartProcess(
                        toolPath: squirrelExe,
                        arguments: $"--releasify {package}{(settings.CreateMsi ? "" : " --no-msi")}",
                        workingDirectory: appDir,
                        redirectOutput: true,
                        timeout: (int)TimeSpan.FromMinutes(5).TotalMilliseconds
                    );
                    var sucess = relProcess?.WaitForExit();
                    if (relProcess?.Output != null)
                    {
                        var exeName = Path.GetFileNameWithoutExtension(relProcess.FileName);
                        foreach (var output in relProcess.Output)
                        {
                            if (output.Type == OutputType.Err)
                                Logger.Error($"{exeName}: {output.Text}");
                            else
                                Logger.Info($"{exeName}: {output.Text}");
                        }
                    }

                    if ((relProcess?.ExitCode ?? 0) != 0)
                        throw new Exception($"Releasifying exits with non-zero ({relProcess?.ExitCode.ToString() ?? "<empty>"}) exit code");
                    if (!(sucess ?? false))
                        throw new Exception("Releasifying took too long and was killed");
                }
            }
        }

        /// <summary>
        /// Deploys packages to the artifactory server
        /// </summary>
        /// <param name="configurator">Ruda configuration functor</param>
        public static async Task DeployRudaPackages(Func<RudaSettings> configurator)
        {
            var settings = configurator();
            foreach (var appDir in Directory.GetDirectories(settings.OutDir.Value))
            {
                Logger.Trace($"Processing '{appDir}'.");
                var releasesDir = (AbsolutePath)appDir / "Releases";
                if (!Directory.Exists(releasesDir))
                {
                    Logger.Error($"Directory '{Path.GetFileName(releasesDir)}' does not exists, cannot deploy");
                    continue;
                }

                var appName = Path.GetFileName(appDir);
                var releasesFile = releasesDir / "RELEASES";
                if (!File.Exists(releasesFile))
                {
                    Logger.Error($"File '{Path.GetFileName(releasesFile)}' does not exists, cannot deploy");
                    continue;
                }

                var url = settings.ArtifactoryUrl.AppendPathSegments(settings.ArtifactoryProjectPath, appName, settings.BranchId, "RELEASES");
                Logger.Trace($"Pushing '{releasesFile}' to '{url}'");
                await url.WithHeader("X-JFrog-Art-Api", settings.ArtifactoryApiKey)
                    .PutAsync(new FileContent(releasesFile)
                    {
                        Headers = { ContentType = MediaTypeHeaderValue.Parse("application/octet-stream") }
                    });
                Logger.Info($"File '{Path.GetFileName(releasesFile)}' was pushed.");

                Logger.Trace($"Looking for packages in '{releasesDir}'");
                var newPackages = Directory.EnumerateFiles(releasesDir)
                    .Where(x => x.EndsWith(".nupkg", StringComparison.InvariantCultureIgnoreCase))
                    .Where(file => {
                        Logger.Trace($"Found package '{file}', opening.");
                        using (var fileStream = File.OpenRead(file))
                        {
                            var pckgVer = new PackageArchiveReader(fileStream, false).NuspecReader.GetVersion();
                            var same = pckgVer.CompareTo(VersionCreation.Version, VersionComparison.VersionRelease) == 0;
                            Logger.Trace($"Package version does {(same ? "" : "not ")}match with expected, package version: '{pckgVer}'.");
                            return same;
                        }
                    }).ToList();

                //Push latest full and delta packages
                foreach (var package in newPackages)
                {
                    url = settings.ArtifactoryUrl.AppendPathSegments(settings.ArtifactoryProjectPath, appName, settings.BranchId, Path.GetFileName(package));
                    Logger.Trace($"Pushing package '{package}' to '{url}'");
                    await url
                        .WithHeader("X-JFrog-Art-Api", settings.ArtifactoryApiKey)
                        .PutAsync(new FileContent(package)
                        {
                            Headers = { ContentType = MediaTypeHeaderValue.Parse("application/octet-stream") }
                        });
                    Logger.Info($"Package '{Path.GetFileName(package)}' was pushed");
                }

                //Push setup
                var setupPath = releasesDir / "Setup.exe";
                if (File.Exists(setupPath))
                {
                    url = settings.ArtifactoryUrl.AppendPathSegments(settings.ArtifactoryProjectPath, appName, settings.BranchId, Path.GetFileName(setupPath));
                    Logger.Trace($"Pushing setup '{setupPath}' to '{url}'");
                    await url
                        .WithHeader("X-JFrog-Art-Api", settings.ArtifactoryApiKey)
                        .PutAsync(new FileContent(setupPath)
                        {
                            Headers = { ContentType = MediaTypeHeaderValue.Parse("application/octet-stream") }
                        });
                    Logger.Info($"Setup '{Path.GetFileName(setupPath)}' was pushed");
                }
            }
        }

        public static async Task RudaCleanupRemoteServer(Func<RudaSettings> configurator)
        {
            var settings = configurator();
            if (settings.BranchToId == null)
                throw new ArgumentNullException(nameof(settings.BranchToId), $"{nameof(settings)}.{nameof(settings.BranchToId)} is not set");

            var repoBranches = GetLocalRepoBrancheIds(settings);
            if (!repoBranches.Any())
            {
                Logger.Warn("Unable to determine remote branches, remote server cleanup will not proceed");
                return;
            }

            foreach (var appDir in Directory.GetDirectories(settings.OutDir.Value))
            {
                var appName = Path.GetFileName(appDir);

                //Delete old packages
                Logger.Info($"Cleaning up remote server, app '{appName}'");
                Logger.Info("Deleting unused channels");
                var artChannels = await GetArtChannels(appName, settings);
                var diff = artChannels.Where(x => !repoBranches.Contains(x)).AsQueryable();
                Logger.Info($"Found artifactory only channels with Ids: {string.Join(", ", diff.Select(x => $"'{x}'"))}");
                foreach (var tbdChannel in diff)
                    await DeleteArtChannel(settings, appName, tbdChannel);

                Logger.Info("Deleting old releases");
                artChannels = await GetArtChannels(appName, settings);
                foreach (var channel in artChannels)
                {
                    Logger.Info($"Processing channel '{channel}'");
                    var (_, entries) = await GetReleaseEntries(appName, settings, channel);
                    var entriesList = entries.ToList();
                    //Releaes * 2 = full+deltas
                    var tbdReleases = entries.OrderByDescending(x => x.Version).Skip(settings.PreserveReleases * 2);
                    foreach (var tbdRelease in tbdReleases)
                    {
                        try
                        {
                            await DeleteReleaseFromArtifactory(settings, appName, channel, tbdRelease);
                            entriesList.Remove(tbdRelease);
                        }
                        catch (Exception e)
                        {
                            Logger.Error($"Unable to delete release '{tbdRelease.Version.ToFullString()}' " +
                                         $"in channel '{channel}', {e}{Environment.NewLine}{e.StackTrace}");
                        }
                    }

                    if (entriesList.Count == entries.Count())
                    {
                        Logger.Trace("No releases removed, skipping RELEASES file upload");
                        continue;
                    }
                    var releasesFile = settings.OutDir.Value / appName / $"releases-{channel}";
                    ReleaseEntry.WriteReleaseFile(entriesList, releasesFile);

                    var url = settings.ArtifactoryUrl.AppendPathSegments(settings.ArtifactoryProjectPath, appName, channel, "RELEASES");
                    Logger.Trace($"Pushing '{releasesFile}' to '{url}'");
                    await url.WithHeader("X-JFrog-Art-Api", settings.ArtifactoryApiKey)
                        .PutAsync(new FileContent(releasesFile)
                        {
                            Headers = { ContentType = MediaTypeHeaderValue.Parse("application/octet-stream") }
                        });
                    Logger.Info($"File '{Path.GetFileName(releasesFile)}' was pushed.");
                }
            }
        }

        static async Task DeleteReleaseFromArtifactory(RudaSettings settings, string appName, string channel, ReleaseEntry tbdRelease)
        {
            var url = settings.ArtifactoryUrl.AppendPathSegments(settings.ArtifactoryProjectPath, appName, channel, tbdRelease.Filename);
            Logger.Trace($"Deleting release '{tbdRelease.Version.ToFullString()}' from '{url}'");
            await url
                .WithHeader("X-JFrog-Art-Api", settings.ArtifactoryApiKey)
                .DeleteAsync();
            Logger.Info($"Release \'{tbdRelease.Version.ToFullString()}\' was deleted");
        }

        static List<string> GetLocalRepoBrancheIds(RudaSettings settings)
        {
            var repoPath = string.Empty;
            FindParentDirectory(settings.SourceDirectory,
                x => {
                    var hit = x.EnumerateDirectories().SingleOrDefault(f => f.Name.EndsWith(".git"));
                    if (hit != null)
                        repoPath = hit.FullName;
                    return hit != null;
                });

            var repoBranches = new List<string>();
            if (string.IsNullOrEmpty(repoPath))
            {
                Logger.Warn("Unable to find repositorory root, remote server cleanup will not proceed");
                return repoBranches;
            }

            Logger.Trace($"Opening repository '{repoPath}'");
            using (var repo = new Repository(repoPath))
            {
                Logger.Trace("Querying remote branches");
                if (repo.Branches.Any(x => x.FriendlyName.StartsWith("origin/")))
                {
                    repoBranches = repo.Branches.Where(b => b.IsRemote).Select(b => b.FriendlyName).ToList();
                    Logger.Trace("Found remote branches with a 'origin/' prefix, removing the prefix");
                    repoBranches = repoBranches.Select(b => b.Replace("origin/", "")).ToList();
                }
                else
                {
                    Logger.Info("Not found any remote branches, considering local as remote ones, a.k.a. bamboo hack ;-)");
                    repoBranches = repo.Branches.Select(b => b.FriendlyName).ToList();
                }
                Logger.Trace($"Found remote branches: {string.Join(", ", repoBranches.Select(x => $"'{x}'"))}");
                repoBranches = repoBranches.Select(settings.BranchToId).ToList();
                Logger.Trace($"Found remote branches with Ids: {string.Join(", ", repoBranches.Select(x => $"'{x}'"))}");
            }

            return repoBranches;
        }

        static async Task<List<string>> GetArtChannels(string appName, RudaSettings settings)
        {
            Logger.Trace($"Downloading list of deployed channels for '{appName}'");
            var dm = new ArtifactoryDownloader(settings.ArtifactoryUrl, settings.ArtifactoryProjectPath.AppendPathSegment(appName));
            var artChannels = new List<string>();
            try
            {
                artChannels = await dm.GetAvailableChannelsAsync();
                Logger.Trace($"Found artifactory channels with Ids: {string.Join(", ", artChannels.Select(x => $"'{x}'"))}");
            }
            catch (Exception e)
            {
                Logger.Warn($"Error getting list of deployed channels for '{appName}'{Environment.NewLine}{e}{Environment.NewLine}{e.StackTrace}");
                return artChannels;
            }

            return artChannels;
        }

        static async Task DeleteArtChannel(RudaSettings settings, string appName, string tbdChannel)
        {
            var url = settings.ArtifactoryUrl.AppendPathSegments(settings.ArtifactoryProjectPath, appName, tbdChannel);
            Logger.Trace($"Deleting channel '{url}'");
            await url
                .WithHeader("X-JFrog-Art-Api", settings.ArtifactoryApiKey)
                .DeleteAsync();
            Logger.Info($"Channel \'{tbdChannel}\' was deleted");
        }

        /// <summary>
        /// Locates squirrel executable in packages folder
        /// </summary>
        /// <param name="squirrelPackageName">Alernative, case insensitive, package name</param>
        public static AbsolutePath LocateSquirrel(string squirrelPackageName = "ysoft.rqa.squirrel.windows")
        {
            var nugetFolder = (AbsolutePath)Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) / ".nuget" / "packages" / squirrelPackageName;
            //Get just the folder names, not the whole path
            var dirs = Directory.EnumerateDirectories(nugetFolder).Select(Path.GetFileName);
            var versions = new List<NuGetVersion>();
            foreach (var dir in dirs)
            {
                if (NuGetVersion.TryParse(dir, out var version))
                    versions.Add(version);
                else
                    Logger.Warn($"Found invalid package versions in path '{nugetFolder}'");
            }

            var lastVersion = versions.OrderBy(x => x).LastOrDefault();
            if (lastVersion == null)
                throw new Exception($"Unable to find Squirrel in '{nugetFolder}'");

            var toolsFolder = nugetFolder / lastVersion.ToFullString() / "tools";
            var squirrelExe = toolsFolder / "Squirrel.exe";

            if (File.Exists(squirrelExe))
                return squirrelExe;
            throw new Exception($"Folder '{toolsFolder}' does not contain Squirrel executable");
        }
    }
}
