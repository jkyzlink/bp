﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using Castle.Facilities.Logging;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using YSoft.Rqa.Deployment.Castle;
using YSoft.Rqa.Deployment.Gui;

namespace YSoft.Rqa.Deployment.UpdateShortcut
{
    class ContainerBootstraper : IDisposable
    {
        private IWindsorContainer _container;

        public IWindsorContainer Bootstrap(bool consoleLogger) {
            _container = new WindsorContainer();
            if (consoleLogger)
                _container.AddFacility<LoggingFacility>(f => f.LogUsing(LoggerImplementation.Console));
            else
                _container.AddFacility<LoggingFacility>(f => f.LogUsing(LoggerImplementation.Trace));
            _container.Register(Component.For<GuiCliExtension>().ImplementedBy<GuiCliExtension>());
            _container.UseRuda();
            return _container;
        }

        public void Dispose() {
            _container?.Dispose();
            _container = null;
        }
    }
}
