﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Diagnostics;
using System.Linq;
using Squirrel;
using YSoft.Rqa.Deployment.Gui;

namespace YSoft.Rqa.Deployment.UpdateShortcut
{
    static class Program
    {
        [STAThread]
        static void Main() {
            if (!Debugger.IsAttached) {
                var mgr = new UpdateManager("");
                SquirrelAwareApp.HandleEvents(
                    onInitialInstall: v => mgr.CreateShortcutForThisExe(),
                    onAppUpdate: v => mgr.CreateShortcutForThisExe(),
                    onAppUninstall: v => mgr.RemoveShortcutForThisExe(),
                    onFirstRun: () => Environment.Exit(0)
                );
            }

            MainAsync();
        }

        private static async void MainAsync() {
            var keepConsole = Environment.GetCommandLineArgs().Skip(1).Contains("-c");
            var bootstraper = new ContainerBootstraper();
            var container = bootstraper.Bootstrap(keepConsole);
            var guiExtension = container.Resolve<GuiCliExtension>();
            if (Debugger.IsAttached)
                await guiExtension.ExecuteAsync(new NullUpdater(null), null, !keepConsole).ConfigureAwait(false);
            else
                await guiExtension.ExecuteAsync(container.Resolve<IUpdater>(), null, !keepConsole).ConfigureAwait(false);
            container.Dispose();
        }
    }
}
