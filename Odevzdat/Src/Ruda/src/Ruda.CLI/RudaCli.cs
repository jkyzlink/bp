﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommandLine;
using NuGet.Versioning;
using ILogger = Castle.Core.Logging.ILogger;
using NullLogger = Castle.Core.Logging.NullLogger;

namespace YSoft.Rqa.Deployment.Cli
{
    /// <summary>
    /// CLI interface to Ruda
    /// </summary>
    public class RudaCli
    {
        private readonly IUpdater _updater;
        private readonly ICollection<ICliExtension> _extendedOpts;
        private readonly ILogger _log;
        private Options.UpdateOptions _updateOpts;
        private Options.InfoOptions _infoOpts;
        private object _parsed;

        /// <summary>
        /// Instantiates new RudaCli
        /// </summary>
        /// <param name="updater">Desired updater</param>
        /// <param name="extendedOpts"></param>
        /// <param name="log">Log</param>
        public RudaCli(IUpdater updater, ICollection<ICliExtension> extendedOpts, ILogger log) {
            _updater = updater;
            _extendedOpts = extendedOpts;
            _log = log ?? NullLogger.Instance;
        }

        /// <summary>
        /// Parse and store input arguments
        /// </summary>
        /// <param name="arguments">Input arguments</param>
        /// <returns>Whether parsing was successful or not <see cref="ExecuteAsync"/></returns>
        // ReSharper disable once UnusedMethodReturnValue.Global
        public bool Parse(IEnumerable<string> arguments) {
            var argsArr = arguments as string[] ?? arguments.ToArray();
            _log.Debug($"Parsing: {string.Join(", ", argsArr)}");

            var optType = new List<Type> {typeof(Options.UpdateOptions), typeof(Options.InfoOptions)};
            if (_extendedOpts != null) {
                var extTypes = _extendedOpts.Select(x => x.OptionType).ToArray();
                _log.Debug($"Cli is extended with \'{string.Join(", ", extTypes.Select(x=>x.FullName))}\'");
                optType.AddRange(extTypes);
            }

            var parserResult = Parser.Default.ParseArguments(argsArr, optType.ToArray());
            parserResult.WithParsed(x => _parsed = x);

            return parserResult.Tag == ParserResultType.Parsed;
        }

        /// <summary>
        /// Executes actions specified by input arguments.
        /// </summary>
        /// <exception cref="InvalidOperationException">When input arguments parsing fail or does not run.</exception>
        public async Task ExecuteAsync() {
            if ((_updateOpts = _parsed as Options.UpdateOptions) != null) {
                try {
                    await ExecuteUpdateAsync().ConfigureAwait(false);
                } catch (Exception e) {
                    _log.Error($"Unable to perform update, check logs for details.\n{e.Message}", e);
                }
            }
            else if ((_infoOpts = _parsed as Options.InfoOptions) != null)
                try {
                    await ExecuteShowAsync().ConfigureAwait(false);
                } catch (Exception e) {
                    _log.Error($"Unable to show info, check logs for details.\n{e.Message}", e);
                }
            else {
                ICliExtension extension;
                try {
                    extension = _extendedOpts.SingleOrDefault(x => x.OptionType == _parsed.GetType());
                } catch (InvalidOperationException) {
                    throw new Exception("Type match more than extension. Invalid commandline argument extensions.");
                }
                if(extension == null)
                    throw new ArgumentException("Parsed arguments have unexpected type.");
                try {
                    await extension.ExecuteAsync(_updater, _parsed).ConfigureAwait(false);
                } catch (Exception e) {
                    _log.Error($"Exception during extension execution.\n{e.Message}", e);
                }
            }
        }

        private async Task ExecuteUpdateAsync() {
            if (_updateOpts.Channel != null)
                await _updater.SetFutureChanelAsync(_updateOpts.Channel).ConfigureAwait(false);

            if (_updateOpts.Latest)
                await _updater.UpdateToLatestAsync(!_updateOpts.NoRestart).ConfigureAwait(false);

            if (!string.IsNullOrEmpty(_updateOpts.Version)) {
                if (!SemanticVersion.TryParse(_updateOpts.Version, out var appSemVersion))
                    throw new ArgumentException($"Unable to parse version to {nameof(SemanticVersion)}.");

                var appVersions = await _updater.PossibleReleasesAsync().ConfigureAwait(false);
                var desiredAppVersion = appVersions.SingleOrDefault(x => x.Version == appSemVersion);
                if (desiredAppVersion == null)
                    throw new ArgumentException("Unable to find this version in repository.");
                await _updater.UpdateToReleaseAsync(desiredAppVersion, !_updateOpts.NoRestart).ConfigureAwait(false);
            }
        }

        private async Task ExecuteShowAsync() {
            if (_infoOpts.Available) {
                if (_infoOpts.Version) {
                    var releases = (await _updater.PossibleReleasesAsync().ConfigureAwait(false)).Select(x => x.Version);
                    Console.WriteLine($"Versions: {string.Join(", ", releases)}");
                }
                if (_infoOpts.Channel)
                    Console.WriteLine($"Channels: {string.Join(", ", await _updater.AvailableChannelsAsync().ConfigureAwait(false))}");
                if (_infoOpts.Latest) {
                    var lastVersion = (await _updater.PossibleReleasesAsync().ConfigureAwait(false)).LastOrDefault()?.Version;
                    Console.WriteLine($"Latest: {lastVersion}");
                }
                if (!_infoOpts.NoPause) {
                    Console.WriteLine("Press any key to exit...");
                    Console.Read();
                }
            }
            else {
                if (_infoOpts.Version)
                    Console.WriteLine($"Version: {_updater.CurrentVersion()}");
                if (_infoOpts.Channel)
                    Console.WriteLine($"Channel: {_updater.CurrentChannel()}");
                if (_infoOpts.Latest) {
                    var newerAvailable = await _updater.NewReleaseAvailableAsync().ConfigureAwait(false);
                    Console.WriteLine($"Latest: {!newerAvailable}");
                }
                if (!_infoOpts.NoPause) {
                    Console.WriteLine("Press any key to exit...");
                    Console.Read();
                }
            }
        }
    }
}
