﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Threading.Tasks;
using CommandLine;

namespace YSoft.Rqa.Deployment.Cli
{
    /// <summary>
    /// Provides cli commands extensibility
    /// </summary>
    public interface ICliExtension
    {
        /// <summary>
        /// Type of the options class that will be used for parsing
        /// For semantics see <see cref="CommandLine"/>, <see cref="Parser"/>
        /// </summary>
        Type OptionType { get; }

        /// <summary>
        /// Invoked when arguments match option file
        /// </summary>
        /// <param name="updater">Updater instance</param>
        /// <param name="options">Options, type of this will be <see cref="OptionType"/>.</param>
        /// <param name="hideConsole">Hides opened console window</param>
        Task ExecuteAsync(IUpdater updater, object options, bool hideConsole = true);
    }
}