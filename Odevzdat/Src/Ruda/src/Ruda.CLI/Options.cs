﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using CommandLine;
// ReSharper disable ClassNeverInstantiated.Global

namespace YSoft.Rqa.Deployment.Cli
{
    internal class Options
    {
        [Verb("update", HelpText = "Update application")]
        public class UpdateOptions
        {
            [Option(
                shortName: 'n',
                longName: "noRestart",
                Required = false,
                Default = false,
                HelpText = "Suppress restart after update. New version will run after manual start")]
            public bool NoRestart { get; set; }

            [Option(
                shortName: 'c',
                longName: "channel",
                Required = false,
                HelpText = "Set update channel")]
            public string Channel { get; set; }

            [Option(
                shortName: 'l',
                longName: "latest",
                Required = false,
                SetName = "latest",
                HelpText = "Update to the latest version")]
            public bool Latest { get; set; }

            [Option(
                shortName: 'v',
                longName: "version",
                SetName = "version",
                HelpText = "Use specific version")]
            public string Version { get; set; }

            [Option(
                shortName: 'p',
                longName: "noPause",
                Required = false,
                HelpText = "Exit as soon as possible, skipping waiting for user input")]
            public bool NoPause { get; set; }
        }

        [Verb("info", HelpText = "Display info about currently installed version")]
        public class InfoOptions
        {
            [Option(
                shortName: 'a',
                longName: "available",
                Default = false,
                HelpText = "Show available channel/version instead of currently installed")]
            public bool Available { get; set; }

            [Option(
                shortName: 'v',
                longName: "version",
                HelpText = "Show semantic version")]
            public bool Version { get; set; }

            [Option(
                shortName: 'c',
                longName: "channel",
                HelpText = "Show channel")]
            public bool Channel { get; set; }

            [Option(
                shortName: 'l',
                longName: "latest",
                HelpText = "Show if the latest version is installed or latest version number")]
            public bool Latest { get; set; }

            [Option(
                shortName: 'p',
                longName: "noPause",
                Required = false,
                HelpText = "Exit as soon as possible, skipping waiting for user input")]
            public bool NoPause { get; set; }
        }
    }
}
