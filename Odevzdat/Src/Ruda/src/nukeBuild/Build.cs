﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.IO;
using System.Linq;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.MSBuild;
using Nuke.Common.Tools.NuGet;
using Nuke.Common;
using Nuke.Common.Tooling;
using Rqa.NukeHelpers;
using static Nuke.Common.Tools.MSBuild.MSBuildTasks;
using static Nuke.Common.Tools.DotNet.DotNetTasks;
using static Nuke.Common.Tools.NuGet.NuGetTasks;
using static Nuke.Common.IO.FileSystemTasks;
using static Nuke.Common.IO.PathConstruction;

// ReSharper disable InconsistentNaming
class Build : NukeBuild
{
    [Parameter("Skip clean during build (use for local builds).")]
    readonly bool NoClean;

    [Parameter("List of nuget source URLs. Separated with ','.", Separator = ",")]
    readonly string[] NugetSources = {"https://api.nuget.org/v3/index.json"};

    readonly string[] Csprojs;

    AbsolutePath NugetsOutputPath => OutputDirectory / "Nugets";

    public Build() {
        Csprojs = GlobFiles(SourceDirectory, "*.csproj")
            .Where(x => !x.Contains(".build.csproj"))
            .ToArray();
    }

    public static int Main() => Execute<Build>(x => x.Create_Nugets);

    Target Clean => _ => _
        .OnlyWhen(() => !NoClean)
        .Executes(() => {
            Verbosity = Verbosity.Verbose;
            MSBuild(x => DefaultMSBuild
                .SetTargets("Clean")
                .SetMSBuildVersion(MSBuildVersion.VS2017)
            );

            var dirs = GlobDirectories(SourceDirectory, "*/bin", "*/obj")
                .Where(x => !x.Contains("nukeBuild"));
            DeleteDirectories(dirs);
            EnsureCleanDirectories(new[] {OutputDirectory.ToString(), NugetsOutputPath});
        });

    Target Restore => _ => _
        .DependsOn(Clean)
        .Executes(() => { NuGetRestore(SolutionDirectory, settings => settings.SetSource(NugetSources)); });

    Target Compile => _ => _
        .DependsOn(Restore)
        .Executes(() => {
            VersionCreation.Type = VersionType.GitBased;
            var ver = new Version(1, 0).ToString();

            MSBuild(x => DefaultMSBuildCompile
                .SetTargets("Build")
                .SetMSBuildVersion(MSBuildVersion.VS2017)
                .SetAssemblyVersion(ver)
                .SetFileVersion(ver)
                .SetInformationalVersion(VersionCreation.Version.ToNormalizedString())
            );
        });

    Target Create_Nugets => _ => _
        .DependsOn(Compile)
        .Executes(() => {
            foreach (var csproj in NugetHelpers.CreateAllNuspecsFromSpecs()) {
                if (SolutionHelpers.IsCoreCsproj(csproj)) {
                    Logger.Info($"Packing netcore project \'{csproj.ProjectName}\'.");
                    DotNetPack(cfg => DefaultDotNetPack
                        .SetProject(csproj.AbsolutePath)
                        .SetWorkingDirectory(Path.GetDirectoryName(csproj.AbsolutePath))
                        .SetOutputDirectory(NugetsOutputPath)
                        .SetVersion(VersionCreation.Version.ToFullString())
                        .SetConfiguration(Configuration)
                        .DisableIncludeSymbols()
                        .SetNoDependencies(csproj.ProjectName.StartsWith("Ruda.UpdateShortcut"))
                    );
                }
                else {
                    Logger.Info($"Packing project \'{csproj.ProjectName}\'.");
                    NuGetPack(cfg => DefaultNuGetPack
                        .SetBasePath(Path.GetDirectoryName(csproj.AbsolutePath))
                        .EnableForceEnglishOutput()
                        .SetTargetPath(csproj.AbsolutePath)
                        .SetOutputDirectory(NugetsOutputPath)
                        .SetVersion(VersionCreation.Version.ToFullString())
                        .SetConfiguration(Configuration)
                        .EnableIncludeReferencedProjects()
                    );
                }
            }
        });
}
