/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using CommandLine;

namespace YSoft.Rqa.Deployment.Gui
{
    /// <summary>
    /// Gui command options
    /// </summary>
    [Verb("gui", HelpText = "Shows gui to perform update")]
    public class GuiCliOptions { }
}
