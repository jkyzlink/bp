﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using NuGet.Versioning;

namespace YSoft.Rqa.Deployment.Gui
{
    /// <summary>
    /// Application version model
    /// </summary>
    // ReSharper disable once ClassWithVirtualMembersNeverInherited.Global
    public class AppVersionModel : INotifyPropertyChanged
    {
        private SemanticVersion _semVer;
        private ObservableCollection<string> _versionMetadata = new ObservableCollection<string>();
        private string _builtDate;
        private bool _highlighted;

        /// <summary>
        /// Full semver compatible version
        /// </summary>
        public SemanticVersion SemVer {
            get => _semVer;
            set {
                _semVer = value;
                VersionMetadata.Clear();
                var metadata = Ruda.MetadataToValuePairs(_semVer).Select(x => $"{x.Key}: {x.Value}").ToList();
                foreach (var mtdt in metadata)
                    VersionMetadata.Add(mtdt);
                BuiltDate = _semVer.ToPublishDate()?.ToString("F") ?? "unknown";
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Metadata as key-value groups
        /// </summary>
        public ObservableCollection<string> VersionMetadata {
            get => _versionMetadata;
            set {
                _versionMetadata = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Stringified build date
        /// </summary>
        public string BuiltDate {
            get => _builtDate;
            set {
                _builtDate = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Should the vesion be highlighted
        /// </summary>
        public bool Highlighted {
            get => _highlighted;
            set {
                _highlighted = value;
                OnPropertyChanged();
            }
        }

        /// <inheritdoc />
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
