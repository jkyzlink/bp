﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using Castle.Core.Logging;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace YSoft.Rqa.Deployment.Gui
{

    /// <summary>
    /// Interaction logic for SelectPackage.xaml
    /// </summary>
    public partial class SelectPackage : IDisposable
    {
        private SelectPackageViewModel ViewModel { get; set; } = new SelectPackageViewModel();

        private readonly ILogger _log;
        private readonly IUpdater _manager;
        private readonly bool _isDevel;
        private readonly string _wndTitle;

        /// <summary>
        /// Graphical user interface for Ruda
        /// </summary>
        /// <param name="updater">updater implementation</param>
        /// <param name="logger">logger</param>
        public SelectPackage(IUpdater updater, ILogger logger) {
            logger = logger ?? new ConsoleLogger(nameof(SelectPackage));
            InitializeComponent();
            DataContext = ViewModel;

            _manager = updater;
            _log = logger;
            _wndTitle = Title;
            if (_manager == null) {
                _log.Debug("has not received valid update manager");
                MessageBox.Show(this,
                    "Application is not properly deployed",
                    _wndTitle,
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                _isDevel = true;
            }
            else
                _log.Debug($"Current version {_manager.CurrentVersion()?.Version?.ToFullString() ?? "<none>"}");
        }

        private void Version_DoubleClick(object sender, RoutedEventArgs e) {
            BUpdateToSelectedClick(null, null);
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e) {
            if (_isDevel) {
                Close();
                return;
            }

            ViewModel.VersionsHidden = true;
            await UpdateChannelsAsync();
            ViewModel.VersionsHidden = false;
        }

        private async Task UpdateChannelsAsync() {
            _log.Debug("Loading channels");
            try {
                var channels = await _manager.AvailableChannelsAsync().ConfigureAwait(false);
                ViewModel.AvailableChannels = new ObservableCollection<string>(channels);
                var channel = _manager.CurrentChannel();
                _log.Debug($"Current channel '{channel}'");
                if (channel == null) {
                    Utils.PerformSafely(Dispatcher, () => cbChannel.SelectedIndex = -1);
                }
                else {
                    var idx = channels.FindIndex(x => x == channel);
                    Utils.PerformSafely(Dispatcher, () => cbChannel.SelectedIndex = idx);
                    _log.Debug($"Selecting '{channel}', idx '{idx}' in {nameof(cbChannel)}");
                }
                _log.Debug("Collection of channels has been populated");
            } catch (Exception ex) {
                _log.Error("Unable to load channels", ex);
                MessageBox.Show(
                    this,
                    "Unable to load channels",
                    _wndTitle,
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                Close();
            }
        }

        private async Task UpdateReleasesAsync() {
            _log.Debug("Loading collection of releases");
            try {
                var releases = (await _manager.PossibleReleasesAsync().ConfigureAwait(true)).OrderByDescending(x => x.Version).ToList();
                _log.Debug($"Collection of releases has been downloaded, ({releases.Count}) items");

                if (!ViewModel.ShowAllVersions)
                    releases = releases.Take(5).ToList();

                ViewModel.Versions = new ObservableCollection<AppVersionModel>(releases.Select(x => new AppVersionModel {
                    SemVer = x.Version,
                }));

                HighlightCurrentRelease();
                _log.Debug("Collection of releases has been populated");
            } catch (Exception ex) {
                _log.Error("Unable to load releases", ex);
                MessageBox.Show(
                    this,
                    "Unable to load releases for the current channel.\nIt may has been deleted, select another one",
                    _wndTitle,
                    MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }

        private void HighlightCurrentRelease() {
            var currVer = _manager.CurrentVersion()?.Version;
            if (currVer == null)
                return;
            var toBeHighlighted = ViewModel.Versions.FirstOrDefault(x => x.SemVer == currVer);
            if (toBeHighlighted != null)
                toBeHighlighted.Highlighted = true;
        }

        private async void CbChannelChanged(object sender, SelectionChangedEventArgs e) {
            try {
                await _manager.SetFutureChanelAsync((sender as ComboBox)?.SelectedValue?.ToString() ?? "").ConfigureAwait(false);
                await UpdateReleasesAsync();
            } catch (Exception ex) {
                _log.Error($"Error occured during {nameof(UpdateReleasesAsync)}", ex);
            }
        }

        /// <inheritdoc />
        protected override void OnClosing(CancelEventArgs e) {
            Dispose();
            base.OnClosing(e);
        }

        /// <inheritdoc />
        public void Dispose() => _manager?.Dispose();

        private async void BUpdateToSelectedClick(object sender, RoutedEventArgs e)
        {
            using (new DisableSender(Dispatcher, this)) {
                ViewModel.VersionsHidden = true;

                _log.Debug($"Button '{(sender as Button)?.Content}' clicked");

                var targetVersion = ViewModel.SelectedVersion?.SemVer;
                try {
                    if (targetVersion == null) {
                        MessageBox.Show(
                            this,
                            "Select version for update",
                            _wndTitle,
                            MessageBoxButton.OK,
                            MessageBoxImage.Hand);
                        return;
                    }

                    await _manager.UpdateToReleaseAsync(new AppVersion {Version = targetVersion}).ConfigureAwait(false);
                } catch (Exception ex) {
                    _log.Error("Error during updating", ex);
                }
                finally {
                    ViewModel.VersionsHidden = false;
                }
            }
        }

        private async void bUpdateLatest_Click(object sender, RoutedEventArgs e) {
            using (new DisableSender(Dispatcher, this)) {
                ViewModel.VersionsHidden = true;

                try {
                    _log.Debug("Application will be auto updated to latest version");
                    await _manager.UpdateToLatestAsync().ConfigureAwait(false);
                } catch (Exception ex) {
                    _log.Error("Error during updating", ex);
                }
                finally {
                    ViewModel.VersionsHidden = false;
                }
            }
        }

        private async void ShowAllChanged(object sender, RoutedEventArgs e) {
            using (new DisableSender(Dispatcher, sender)) {
                ViewModel.VersionsHidden = true;
                await UpdateReleasesAsync();
                ViewModel.VersionsHidden = false;
            }
        }
    }

    internal class DisableSender : IDisposable
    {
        private readonly Dispatcher _dispatcher;
        private readonly Control _control;

        public DisableSender(Dispatcher dispatcher, object sender) {
            _dispatcher = dispatcher;
            _control = sender as Control;

            if (_control != null)
                Utils.PerformSafely(_dispatcher, () => _control.IsEnabled = false);
        }

        public void Dispose() {
            if (_control != null)
                Utils.PerformSafely(_dispatcher, () => _control.IsEnabled = true);
        }
    }

    internal static class Utils
    {
        public static void PerformSafely(Dispatcher dispatcher, Action action, bool async = false) {
            if (!dispatcher.CheckAccess())
                if (async)
                    dispatcher.InvokeAsync(action);
                else
                    dispatcher.Invoke(action);
            else
                action();
        }
    }
}
