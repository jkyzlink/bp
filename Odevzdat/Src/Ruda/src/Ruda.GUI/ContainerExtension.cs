﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;
using YSoft.Rqa.Deployment.Cli;

// ReSharper disable UnusedMember.Global
namespace YSoft.Rqa.Deployment.Gui
{
    /// <summary>
    /// Easily add cli gui extension to the castle
    /// </summary>
    // ReSharper disable once UnusedMember.Global
    public static class ContainerExtension
    {
        /// <summary>
        /// Register CLI extension (<see cref="ICliExtension"/>) that
        /// shows gui update manager when 'gui' argument is used.
        /// Take care of <see cref="CollectionResolver"/> registration!
        /// </summary>
        /// <param name="container">Windsor container</param>
        /// <param name="addSubResolver">
        /// Whether to add subresolver <see cref="CollectionResolver"/> to the castle's kernel.
        /// GUI will not work if <see cref="CollectionResolver"/> was not added nor was added after.
        /// <c>True</c> to register subresolver.
        /// <c>False</c> to skip subresolver registration.
        /// </param>
        public static void UseRudaGui(this IWindsorContainer container, bool addSubResolver) {
            if (addSubResolver)
                container.Kernel.Resolver.AddSubResolver(new CollectionResolver(container.Kernel));

            container.Register(
                Component
                    .For<ICliExtension>()
                    .ImplementedBy<GuiCliExtension>()
                    .Named(nameof(GuiCliExtension))
            );
        }
    }
}