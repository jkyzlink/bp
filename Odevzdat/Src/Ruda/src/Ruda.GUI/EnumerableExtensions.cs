﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System.Collections.Generic;
using System.Linq;

namespace YSoft.Rqa.Deployment.Gui
{
    internal static class EnumerableExtensions
    {
        public static IEnumerable<TSource> TakeLast<TSource>(this IEnumerable<TSource> enumerable, int count) {
            return enumerable.Reverse().Take(count).Reverse();
        }
    }
}