﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System.Runtime.InteropServices;

namespace YSoft.Rqa.Deployment.Gui
{
    internal static class NativeMethods
    {
        [DllImport("kernel32.dll")]
        internal static extern bool FreeConsole();
    }
}