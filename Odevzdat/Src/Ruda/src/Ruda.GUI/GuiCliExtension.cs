﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.Core.Logging;
using YSoft.Rqa.Deployment.Cli;

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
namespace YSoft.Rqa.Deployment.Gui
{
    public class GuiCliExtension : ICliExtension
    {
        public Type OptionType => typeof(GuiCliOptions);

        private readonly ILogger _log;

        public GuiCliExtension(ILogger log) {
            _log = log ?? new NullLogger();
        }

        [STAThread]
        public async Task ExecuteAsync(IUpdater updater, object options, bool hideConsole = true) {
            await Task.CompletedTask;
            Application.EnableVisualStyles();
            if (hideConsole)
                NativeMethods.FreeConsole();
            new SelectPackage(updater, _log).ShowDialog();
        }
    }
}