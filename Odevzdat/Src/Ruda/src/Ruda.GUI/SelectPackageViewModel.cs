﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace YSoft.Rqa.Deployment.Gui
{
    /// <summary>
    /// SelectPackage.xaml ViewModel
    /// </summary>
    // ReSharper disable once ClassWithVirtualMembersNeverInherited.Global
    public class SelectPackageViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<AppVersionModel> _versions = new ObservableCollection<AppVersionModel>();
        private bool _versionsHidden = false;
        private string _selectedChannel = string.Empty;
        private ObservableCollection<string> _availableChannels = new ObservableCollection<string>();
        private AppVersionModel _selectedVersion;
        private bool _showAllVersions;

        /// <summary>
        /// Available versions
        /// </summary>
        public ObservableCollection<AppVersionModel> Versions {
            get => _versions;
            set {
                _versions = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Overlay versions with loading animation
        /// </summary>
        public bool VersionsHidden {
            get => _versionsHidden;
            set {
                _versionsHidden = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Show all versions checkbox
        /// </summary>
        public bool ShowAllVersions {
            get => _showAllVersions;
            set {
                _showAllVersions = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Currently selected channel
        /// </summary>
        public string SelectedChannel {
            get => _selectedChannel;
            set {
                _selectedChannel = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// All available channels
        /// </summary>
        public ObservableCollection<string> AvailableChannels {
            get => _availableChannels;
            set {
                _availableChannels = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Currently selected version
        /// </summary>
        public AppVersionModel SelectedVersion {
            get => _selectedVersion;
            set {
                _selectedVersion = value;
                OnPropertyChanged();
            }
        }

        /// <inheritdoc />
        public event PropertyChangedEventHandler PropertyChanged;


        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
