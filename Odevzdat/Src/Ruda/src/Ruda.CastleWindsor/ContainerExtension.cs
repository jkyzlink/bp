﻿/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */

using System;
using System.Configuration;
using Castle.Core.Logging;
using Castle.MicroKernel.Registration;
using Castle.Windsor;

// ReSharper disable UnusedMember.Global
namespace YSoft.Rqa.Deployment.Castle
{
    /// <summary>
    /// Easily add updater extension to windsor container
    /// </summary>
    // ReSharper disable once UnusedMember.Global
    public static class ContainerExtension
    {
        /// <summary>
        /// Register <see cref="IUpdater"/> interface in container.
        /// Uses appconfig values for configuration, add these keys to your appconfig
        /// <code> <![CDATA[ 
        /// <add key="Updater.Nexus.Url" value="http://nexus/" />
        /// <add key="Updater.Nexus.GroupId" value="com.ysoft.robolab" />
        /// <add key="Updater.Nexus.ApplicationId" value="robolabApplication" />
        /// ]]> </code>
        /// </summary>
        /// <param name="container">Windsor container</param>
        /// <param name="dLoader">(optional) Custom channeled downloader</param>
        public static void UseRuda(this IWindsorContainer container, IChanneledDownloader dLoader = null) {
            var disable = ConfigurationManager.AppSettings["Updater.Disabled"];
            if (disable != null) {
                // disable defined
                container.Register(
                    Component.For<IUpdater>()
                        .ImplementedBy<NullUpdater>()
                );
                return;
            }

            if (!RegisterDownloader(container))
                return;

            if (!TryRegisterRuda(container, dLoader)) {
                container.Register(
                    Component.For<IUpdater>()
                        .ImplementedBy<NullUpdater>()
                        .IsDefault()
                );
            }
        }

        private static bool TryRegisterRuda(IWindsorContainer container, IChanneledDownloader dLoader) {
            try {
                if (dLoader == null) {
                    container.Register(
                        Component.For<IUpdater>()
                            .ImplementedBy<Ruda>()
                    );
                }
                else {
                    container.Register(
                        Component.For<IUpdater>()
                            .ImplementedBy<Ruda>()
                            .DependsOn(
                                Dependency.OnValue<IChanneledDownloader>(dLoader)
                            )
                    );
                }
                container.Resolve<IUpdater>();
                return true;
            } catch (Exception e) {
                var log = container.Resolve<ILogger>();
                log.Warn("Unable to start Updater is the application properly deployed?", e);
                return false;
            }
        }

        private static bool RegisterDownloader(IWindsorContainer container) {
            var baseUrl = ConfigurationManager.AppSettings["Updater.Artifactory.Url"];
            var projectPath = ConfigurationManager.AppSettings["Updater.Artifactory.projectPath"];
            if (baseUrl == null || projectPath == null) {
                var log = container.Resolve<ILogger>();
                log.Error("Updater setting are incorrect, updater wont work");
                return false;
            }

            container.Register(
                Component
                    .For<IChanneledDownloader>()
                    .ImplementedBy(typeof(ArtifactoryDownloader))
                    .DependsOn(Dependency.OnValue("baseUrl", baseUrl))
                    .DependsOn(Dependency.OnValue("projectPath", projectPath))
            );
            return true;
        }
    }
}