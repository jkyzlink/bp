﻿using System.Text.RegularExpressions;
using NuGet.Versioning;

namespace Squirrel
{
    public static class VersionExtensions
    {
        static readonly Regex _suffixRegex = new Regex(@"(-full|-delta)?\.nupkg$", RegexOptions.Compiled);

        const string _versionRegexStr = @"(?:(?:0|(?:[1-9]\d*)))\.(?:(?:0|(?:[1-9]\d*)))\.(?:(?:0|(?:[1-9]\d*)))(?:-(?:[0-9A-Za-z-]+(?:\.[0-9A-Za-z-]+)*))?(?:\+(?:[0-9A-Za-z-]+(?:\.[0-9A-Za-z-]+)*))?$";
        static readonly Regex _versionRegex = new Regex(_versionRegexStr, RegexOptions.Compiled);

        public static SemanticVersion ToSemanticVersion(this IReleasePackage package)
        {
            return package.InputPackageFile.ToSemanticVersion();
        }

        public static SemanticVersion ToSemanticVersion(this string fileName)
        {
            var name = _suffixRegex.Replace(fileName, "");
            var version = _versionRegex.Match(name).Value;
            return SemanticVersion.Parse(version);
        }
    }

    public static class StringExtensions
    {
        /// <summary>
        /// Returns null if the string is null or empty
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string NullIfEmpty(this string s) => string.IsNullOrEmpty(s) ? null : s;
    }
}
