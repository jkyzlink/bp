/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */
 
 namespace RMSWeb.Model.Roh
{
    public class RohServiceDetail : DetailBase<RohServiceDetailModel>
    {
        public override void PreRender() { }

        public override void Load() { }

        public override void Init() { }

        public override void LoadDetail(RohServiceDetailModel t) {
            LoadedFor = t;
            IsLoaded = true;
        }
    }
}