/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */
 
 using System.Collections.Generic;

namespace RMSWeb.Model.Roh
{
    public class RohServiceInstanceModel
    {
        public string ServiceInstanceId { get; set; }

        public string Endpoint { get; set; }

        public bool Passing { get; set; }

        public List<string> Tags { get; set; }
    }
}