/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */
 
 using System.Collections.Generic;

namespace RMSWeb.Model.Roh
{
    public class RohServiceDetailModel
    {
        public string ServiceName { get; set; }

        public List<RohServiceInstanceModel> Instances { get; set; }
    }
}