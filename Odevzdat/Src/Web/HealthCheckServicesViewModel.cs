/* Copyright (c) 2018 Jiří Kyzlink
 * This file is part of the Bachelor thesis
 */
 
 using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DotVVM.Framework.ViewModel;
using RMSWeb.Model.Roh;
using YSoft.Rqa.HealthCheck.Core;
using YSoft.Rqa.HeatlhCheck.Client;

namespace RMSWeb.ViewModels
{
    public class HealthCheckServicesViewModel : MasterPageViewModel
    {
        public RohServiceDetail ServiceDetail { get; set; }

        [Bind(Direction.None)]
        public IRohClient Client { get; set; }

        public List<RohServiceModel> Services { get; set; }

        public HealthCheckServicesViewModel(RohServiceDetail serviceDetail) {
            ServiceDetail = serviceDetail;
            BrowserTitle = "Health Check";
            HeaderTitle = BrowserTitle;
        }

        public override async Task Init() {
            Services = (await Client.Services.All()).Select(x=>new RohServiceModel{ServiceId = x.ServiceId.ServiceId}).ToList();
            ServiceDetail.CloseDetail = CloseDetail;
            await base.Init();
        }

        private async Task CloseDetail() {
            await Task.CompletedTask;
            ServiceDetail.IsDetailShowed = false;
        }

        public async Task LoadDetail(string serviceId) {
            var service = RohIdParser.Parse<RohServiceId>(serviceId);
            ServiceDetail.LoadDetail(new RohServiceDetailModel {
                ServiceName = serviceId,
                Instances = (await Client.Services.Service(service)
                    .ConfigureAwait(false))
                    .Select(Mapper.Map<RohServiceInstanceModel>).ToList()
            });
            ServiceDetail.IsDetailShowed = true;
        }
    }
}
