# Obsah adresáře

* Soubor Read Me - právě čtete

## Složka Bin

Obsahuje sestavené projekty i NuGet balíčky

* DemoApp - Demonstrační aplikace
* RoH - Framework pro kontrolu stavu aplikací a služeb
* Ruda - Framework pro správu verzí aplikací

## Složka Docs

Obsahuje zdrojové kódy i sestavenou dokumentaci.

## Složka Src

Obsahuje zdrojové kódy vytvořených frameworků, modifikované knihovny Squirrel.Windows a zdrojové soubory týkající se webového rozhraní

* DemoApp - Demonstrační aplikace
* RoH - Framework pro kontrolu stavu aplikací a služeb
* Ruda - Framework pro správu verzí aplikací
* Squirrel.Windows - Modifikovaný framework
* Web - Implementovaná část webového rozhraní

---

## Licence jednotlivých frameworků, knihoven a programů

* DemoApp - MIT (Platí pouze pro zdrojové soubory obsahující komentář informující o příslušnosti k této bakalářské práci.)
* Squirrel.Windows - MIT
* Ostatní se řídí platným autorským zákonem